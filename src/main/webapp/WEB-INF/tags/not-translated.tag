<%@ tag description="Display the non-trasnslated message" pageEncoding="UTF-8" %>
<%@ tag body-content="tagdependent" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="alert alert-warning translationmissing">
	<spring:message code="i18n.content-not-translated" />
</div>
