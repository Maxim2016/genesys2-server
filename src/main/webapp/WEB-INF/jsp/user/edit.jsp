<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
<title><spring:message code="userprofile.update.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="userprofile.update.title" />
	</h1>

	<c:if test="${not empty emailError}">
		<div class="alert alert-danger">${emailError}</div>
	</c:if>
	<form role="form" class="form-horizontal validate" action="<c:url value="/profile/${user.uuid}/update" />" method="post">
		<div class="form-group">
			<label for="name" class="col-lg-2 control-label"><spring:message code="registration.full-name" /></label>
			<div class="col-lg-3">
				<form:input id="name" name="name" class="span3 form-control" path="user.name" />
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-lg-2 control-label"><spring:message code="registration.email" /></label>
			<div class="col-lg-3">
				<form:input id="email" name="email" class="span3 form-control" path="user.email" />
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-lg-2 control-label"><spring:message code="registration.password" /></label>
			<div class="col-lg-3">
				<input type="password" id="password" name="pwd1" class="span3 form-control" autocomplete="off" />
			</div>
		</div>
		<div class="form-group">
			<label for="confirm_password" class="col-lg-2 control-label"><spring:message code="registration.confirm-password" /></label>
			<div class="col-lg-3">
				<input type="password" id="confirm_password" name="pwd2" class="span3 required form-control" autocomplete="off" equalTo="#pwd1" />
			</div>
		</div>


		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" /> <a class="btn btn-default" href="<c:url value="/profile/${user.uuid}" />" class="btn"> <spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<security:authorize access="hasRole('ADMINISTRATOR')">
	<h1>
		<spring:message code="user.roles" />
	</h1>
	<form role="form" class="form-horizontal validate" action="<c:url value="/profile/${user.uuid}/update-roles" />" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<c:forEach items="${availableRoles}" var="role">
		<div class="form-group">
			<div class="col-lg-12">
				<label><input type="checkbox" name="role" value="${role}" ${user.hasRole(role) ? 'checked="true"' : ''} /> ${role}</label>
			</div>
		</div>
		</c:forEach>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
			</div>
		</div>
	</form>
	</security:authorize>
</body>
</html>