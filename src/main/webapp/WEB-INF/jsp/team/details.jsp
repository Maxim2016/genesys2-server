<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
    <title><spring:message code="team.page.profile.title" arguments="${team.name}" argumentSeparator="||" /></title>
</head>
<body>
<h1>
    <c:out value="${team.name}" />
</h1>
<a href="<c:url value="/team/${team.uuid}/edit" />" class="close"><spring:message code="edit" /></a>
<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#team, 'WRITE')">
    <a href="<c:url value="/acl/${team['class'].name}/${team.id}/permissions"><c:param name="back">/team/${team.uuid}</c:param></c:url>" class="btn btn-default"> <spring:message code="edit-acl" /></a>
</security:authorize>

<h4>
    <spring:message code="team.team-members" arguments="${teammembers.size()}" />
</h4>



<ul class="funny-list">
    <c:forEach items="${teammembers}" var="user" varStatus="status">
        <li class="${status.count % 2 == 0 ? 'even' : 'odd'}">
            <c:out value="${user.name}" />
            <security:authorize access="hasRole('ADMINISTRATOR')">
                <a href="<c:url value="/team/${team.uuid}/${user.uuid}/deleteMember"/> "><spring:message code="delete"/></a>
            </security:authorize>
        </li>
    </c:forEach>
</ul>
<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#team, 'WRITE')">
    <form action="<c:url value="/team/${team.uuid}/addMember"/>" class="form-horizontal">
        <div class="form-group">
            <label for="email" class="col-lg-2 control-label"><spring:message code="team.user.enter.email"/></label>
            <div class="col-lg-3"><input type="text" id="email" name="email" class="form-control"/></div>
            <div class="col-lg-3"><input type="submit" class="btn btn-green" value="<spring:message code="add"/>"/>
            </div>
        </div>
    </form>
</security:authorize>

<c:if test="${error ne null}">
    <div class="alert alert-danger"><spring:message code="user.not.found"/></div>
</c:if>

<div class="audit-info">
		<c:if test="${team.lastModifiedBy ne null}"><spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(team.lastModifiedBy)}" /></c:if>
		<fmt:formatDate value="${team.lastModifiedDate}" type="both" />
	</div>
</body>
</html>