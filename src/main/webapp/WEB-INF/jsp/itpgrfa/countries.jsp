<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="itpgrfa.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="itpgrfa.page.list.title" />
	</h1>

	<div class="main-col-header">
		<a href="<c:url value="/explore"><c:param name="filter" value='{"mls":[true]}' /></c:url>"><spring:message code="view.accessions" /></a>
	</div>
	
	<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>


	<div id="letter-top" class="main-col-header">
	<c:set value="" var="hoofdleter" />
	<c:forEach items="${countries}" var="country" varStatus="status">
		<c:if test="${hoofdleter ne country.getName(pageContext.response.locale).substring(0, 1)}">
			<c:set var="hoofdleter" value="${country.getName(pageContext.response.locale).substring(0, 1)}" />
			<a class="letter-pointer" href="#letter-${hoofdleter}"><c:out value="${hoofdleter}" /></a>
		</c:if>
	</c:forEach>
	</div>

	<div class="nav-header">
		<spring:message code="paged.totalElements" arguments="${countries.size()}" />
	</div>

	<c:set value="" var="hoofdleter" />
	<ul class="funny-list">
		<c:forEach items="${countries}" var="country" varStatus="status">
			<c:if test="${hoofdleter ne country.getName(pageContext.response.locale).substring(0, 1)}">
				<c:set var="hoofdleter" value="${country.getName(pageContext.response.locale).substring(0, 1)}" />
				<li id="letter-${hoofdleter}" class="hoofdleter"><c:out value="${hoofdleter}" />
					<small><a href="#letter-top"><spring:message code="jump-to-top" /></a></small>
				</li>
			</c:if>
			<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show ${not country.current ? 'disabled' : ''}" href="<c:url value="/geo/${country.code3}" />"><c:out value="${country.getName(pageContext.response.locale)}" /></a></li>
		</c:forEach>
	</ul>
	<c:remove var="hoofdleter" />

</body>
</html>