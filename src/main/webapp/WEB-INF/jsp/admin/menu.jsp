
<div class="main-col-header clearfix">
  <div class="nav-header pull-left">
    <a class="" href="<c:url value="/admin/cache/" />"><spring:message code="menu.admin.caches" /></a> <a class=""
      href="<c:url value="/admin/logger/" />"
    ><spring:message code="menu.admin.loggers" /></a> <a class="" href="<c:url value="/admin/ds2/" />"><spring:message
        code="menu.admin.ds2"
      /></a> <a class="" href="<c:url value="/admin/kpi/" />"><spring:message code="menu.admin.kpi" /></a>
		<a href="<c:url value="/admin/elastic/" />" class="">Elasticsearch</a>
  </div>
</div>
