<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
  <head>
    <title><spring:message code="admin.page.title" /></title>
  </head>
  <body>
    <%@ include file="/WEB-INF/jsp/admin/menu.jsp"%>

		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
			<input type="text" name="filter" placeholder="Genesys filter {}" value="{}" />
			<button type="submit" class="btn btn-default" name="reindex">Reindex</button>
			<button type="submit" class="btn btn-default" name="regenerate">Regenerate</button>
		</form>
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<button type="submit" class="btn btn-default" name="clear-queues">Clear ES update queues</button>
		</form>
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			Alias:<input type="text" name="aliasName" />
			Index:<input type="text" name="indexName" />
			<button type="submit" class="btn btn-primary" value="realias" name="action">Move alias</button>
			<button type="submit" class="btn btn-default" value="delete-alias" name="action">Delete alias</button>
		</form>

	<h3>Indexes and their aliases</h3>
    <c:if test="${empty indexes}">
      <h5>No indexes found.</h5>
    </c:if>
    <c:forEach items="${indexes}" var="indexMap" varStatus="i">
      <div>Index #${i.count}: ${indexMap.key}</div>
      <c:choose>
        <c:when test="${empty indexMap.value}">
          <h5>No aliases found.</h5>
        </c:when>
        <c:otherwise>
          <table>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Filter</th>
              <th>Index routing</th>
              <th>Search routing</th>
            </tr>
            <c:forEach items="${indexMap.value}" var="alias" varStatus="j">
              <tr>
                <td>${j.count}</td>
                <td>${alias.alias}</td>
                <td>${alias.filter}</td>
                <td>${alias.indexRouting}</td>
                <td>${alias.searchRouting}</td>
              </tr>
            </c:forEach>
          </table>
        </c:otherwise>
      </c:choose>
      <br/>
    </c:forEach>
  </body>
</html>