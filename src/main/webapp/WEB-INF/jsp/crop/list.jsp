<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="crop.croplist" /></title>
</head>
<body>
	<h1>
		<spring:message code="crop.croplist" />
	</h1>

	<ul class="funny-list">
		<c:forEach items="${crops}" var="crop" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<div class="row">
					<div class="col-xs-4">
						<a class="show pull-left" href="<c:url value="/c/${crop.shortName}" />"><c:out value="${crop.getName(pageContext.response.locale)}" /></a>
					</div>
					<div class="col-xs-8">
						<span style="margin: 0 1em; white-space: nowrap;"><c:out value="${crop.shortName}" /></span>
 						<c:forEach items="${crop.otherNames}" var="otherName">
							<span style="margin: 0 1em; white-space: nowrap;"><c:out value="${otherName}" /></span>
						</c:forEach>
					</div>
				</div>
			</li>
		</c:forEach>
	</ul>

</body>
</html>