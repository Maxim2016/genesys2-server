<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
	<title><c:out value="${crop.getName(pageContext.response.locale)}" /></title>
	<meta name="description" content="<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body>
<c:if test="${crop eq null}">
	<div class="alert alert-error">
		<spring:message code="data.error.404"/>
	</div>
</c:if>

<div class="informative-h1 row">
		<div class="col-md-12 col-sm-12">
			<h1>
				<c:out value="${crop.getName(pageContext.response.locale)}"/>
			</h1>
			<c:out value="${blurp.summary}" escapeXml="false" />
		</div>
</div>

<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')">
	<a href="<c:url value="/acl/${crop.getClass().name}/${crop.id}/permissions"><c:param name="back"><c:url value="/c/${crop.shortName}" /></c:param></c:url>"
	   class="close">
		<spring:message code="edit-acl"/>
	</a>
</security:authorize>
<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#crop, 'ADMINISTRATION')">
	<a href="<c:url value="/c/${crop.shortName}/edit" />" class="close">
		<spring:message code="edit"/>
	</a>
</security:authorize>

<c:if test="${blurp ne null}">
		<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp" %>
</c:if>


	<div class="content-section-2015">
		<h3>
			<span>
				<spring:message code="heading.general-info" />
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<ul class="funny-list statistics">
					<li class="clearfix odd">
						<span class="stats-number">
							<fmt:formatNumber value="${cropCount}" />
						</span>
						<spring:message code="faoInstitutes.stat.accessionCount" />
					</li>
				</ul>
				<div class="row" style="margin-top: 2em;">
					<div class="col-sm-12">
						<a class="btn btn-primary" title="" href="<c:url value="/c/${crop.shortName}/data" />">
							<span class="glyphicon glyphicon-list"></span>
							<spring:message code="view.accessions" />
						</a>
						
						<a class="btn btn-default" title="" href="<c:url value="/c/${crop.shortName}/overview" />">
							<span class="glyphicon glyphicon-eye-open"></span>
							<spring:message code="data-overview.short" />
						</a>

						<a class="btn btn-default" href="<c:url value="/c/${crop.shortName}/descriptors" />"><spring:message code="crop.view-descriptors"/></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="content-section-2015">
		<h3>
			<span>
				<spring:message code="chart.collections.title" />
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<div id="cropMap" style="min-height: 500px; min-width: 200px; margin: 0 auto"></div>
			</div>
		</div>
	</div>

	<div class="content-section-2015">
		<h3>
			<span>
				<spring:message code="crop.taxonomy-rules"/>
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<ul class="funny-list">
					<c:forEach items="${cropRules}" var="rule">
						<li class="${rule.included ? '' : 'excluded'}"/>
						<b>${rule.included ? '+' : '-'}</b> <c:out value="${rule.genus}"/> <c:out
							value="${rule.species eq null ? '*' : rule.species}"/></li>
					</c:forEach>
				</ul>
				
			<%-- 	<h3><spring:message code="taxonomy-list"/></h3> --%>
				<ul class="funny-list">
					<c:forEach items="${cropTaxonomies.content}" var="cropTaxonomy" varStatus="status">
						<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a
								href="<c:url value="/acn/t/${cropTaxonomy.taxonomy.genus}/${cropTaxonomy.taxonomy.species}" />"><c:out
								value="${cropTaxonomy.taxonomy.taxonName}"/></a></li>
					</c:forEach>
					<c:if test="${cropTaxonomies.hasNext()}">
						<li id="loadMoreTaxonomies">
							<button class="btn"><spring:message code="label.load-more-data" /></button>
						</li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	
<content tag="javascript">
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$("body").on("click", "#loadMoreTaxonomies", function (event) {
				event.preventDefault();
				var loader = $(this);
				var page = loader.attr("page") || 2;
				$.ajax({
					url: "<c:url value="/c/${crop.shortName}/ajax/taxonomies" />",
					type: "GET",
					data: {
						"page": page
					},
					success: function (data) {
						loader.before(data);
						loader.attr("page", parseInt(page) + 1);
					},
					error: function (err) {
						loader.remove();
					}
				});
			});
		});
	</script>

    <script type="text/javascript" src="<c:url value="/html/js/genesyshighcharts.min.js" />"></script>
    <script type="text/javascript">
        $(function () {
            'use strict';

            $.getJSON('/${pageContext.response.locale}/explore/0/charts/data/country-collection-size?filter=${jsonFilter}', function (data) {
                var mapData = Highcharts.geojson(Highcharts.maps['custom/world']);

                $('#cropMap').highcharts('Map', {
                    chart: {
                        borderWidth: 0
                    },

                    title: {
                        text: '<spring:message code="chart.collections.title" />'
                    },
<%--
                    subtitle: {
                        text: '<spring:message code="chart.attribution-text" />' 
                    },
--%>
                    legend: {
                        enabled: false
                    },

                    mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                            verticalAlign: 'top'
                        }
                    },

                    series: [{
                        name: 'World',
                        mapData: mapData,
                        color: '#88ba42',
                        enableMouseTracking: false
                    }, {
                        type: 'mapbubble',
                        mapData: mapData,
                        name: '<spring:message code="chart.collections.series" />',
                        joinBy: ['iso-a2', 'code'],
                        data: data,
                        sizeBy: 'width',
                        color: '#88ba42',
                        minSize: '3',
                        maxSize: '30%',
                        tooltip: {
                            pointFormat: '{point.country}: {point.z}'
                        }
                    }]
                });
            });
        });
    </script>
</content>
</body>
</html>