/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

/**
 * @author mobreza
 */
public interface Api1Constants {
	public static interface Accession {

		public static final String CROP_NAMES = "crops";

		/**
		 * WIEWS code of accession holding institute.
		 */
		public static final String INSTCODE = "instCode";

		/**
		 * Accession number.
		 */
		public static final String ACCENUMB = "acceNumb";

		/**
		 * Used to assign new ACCENUMB
		 */
		public static final String ACCENUMB_NEW = "newAcceNumb";

		/**
		 * Reported genus of accession.
		 */
		public static final String GENUS = "genus";

		/**
		 * May be used to assign a different genus.
		 */
		public static final String GENUS_NEW = "newGenus";

		/**
		 * Specific epithet portion of the scientific name, in latin, in
		 * lowercase letters. Following abbreviation is allowed: ‘sp.’
		 */
		public static final String SPECIES = "species";

		/**
		 * Corresponds to MCPD SPAUTHOR
		 */
		public static final String SPAUTHOR = "spauthor";

		/**
		 * Corresponds to MCPD SUBTAXA
		 */
		public static final String SUBTAXA = "subtaxa";

		/**
		 * Corresponds to MCPD SUBTAUTHOR
		 */
		public static final String SUBTAUTHOR = "subtauthor";

		/**
		 * Universally Unique IDentifier for the accession, assigned by the
		 * first holding institute and immutable when accession is duplicated in
		 * another institute.
		 */
		public static final String UUID = "uuid";

		/**
		 * ISO3 country code of country of origin.
		 */
		public static final String ORIGCTY = "orgCty";

		/**
		 * Date on which the accession entered the collection as YYYYMMDD.
		 * Missing data (MM or DD) should be indicated with hyphens. Leading
		 * zeros are required.
		 */
		public static final String ACQDATE = "acqDate";

		/**
		 * The status of an accession with regards to the Multilateral System
		 * (MLS) of the International Treaty on PlantGenetic Resources for Food
		 * and Agriculture.
		 */
		public static final String MLSSTAT = "mlsStat";

		/**
		 * Is the accession “FAO In trust”
		 */
		public static final String INTRUST = "inTrust";

		/**
		 * The general availability of the accession for distribution, but
		 * subject to current stock.
		 */
		public static final String AVAILABLE = "available";

		/**
		 * MCPD Type of germplasm storage (Corresponds to MCPD STORAGE field).
		 */
		public static final String STORAGE = "storage";

		/**
		 * MCPD Sample status
		 */
		public static final String SAMPSTAT = "sampStat";

		/**
		 * FAO Institute Code (WIEWS code) of the institute where a safety
		 * duplicate of the accession is maintained.
		 */
		public static final String DUPLSITE = "duplSite";

		/**
		 * Corresponds to BREDCODE.
		 */
		public static final String BREDCODE = "bredCode";

		/**
		 * Corresponds to ANCEST.
		 */
		public static final String ANCEST = "ancest";

		/**
		 * FAO WIEWS code of the donor institute
		 */
		public static final String DONORCODE = "donorCode";

		/**
		 * Accession number at donor institute.
		 */
		public static final String DONORNUMB = "donorNumb";

		/**
		 * Name of the donor institute (or person). Provide only when donorCode
		 * is not available.
		 */
		public static final String DONORNAME = "donorName";

		/**
		 * Object containing the collecting data
		 */
		public static final String COLL = "coll";

		/**
		 * Object containing georeference data
		 */
		public static final String GEO = "geo";

		/**
		 * MCPD Remarks
		 */
		public static final String REMARKS = "remarks";

		/**
		 * Marks if accession record is about a historic holding and the
		 * accession no longer exists.
		 */
		public static final String HISTORIC = "historic";

		/**
		 * Accession name
		 */
		public static final String ACCENAME = "acceName";

		/**
		 * Other numbers
		 */
		public static final String OTHERNUMB = "otherNumb";

		/**
		 * URL linking to additional data about the accession either in the
		 * holding genebank or from another source.
		 */
		public static final String ACCEURL = "acceUrl";

		/**
		 * Gene bank provided MCPD#CROPNAME
		 */
		public static final String CROPNAME = "cropName";

	}

	public static interface Institute {
		/**
		 * Corresponds to INSTCODE
		 */
		public static final String INSTCODE = "code";

		public static final String INSTCODE_COUNTRY = "country.iso3";

	}

	public static interface Taxonomy {

		public static final String SCIENTIFIC_NAME = "sciName";

		/**
		 * Reported genus of accession.
		 */
		public static final String GENUS = "genus";

		/**
		 * May be used to assign a different genus.
		 */
		public static final String GENUS_NEW = "newGenus";

		/**
		 * Specific epithet portion of the scientific name, in latin, in
		 * lowercase letters. Following abbreviation is allowed: ‘sp.’
		 */
		public static final String SPECIES = "species";

		/**
		 * Corresponds to MCPD SPAUTHOR
		 */
		public static final String SPAUTHOR = "spauthor";

		/**
		 * Corresponds to MCPD SUBTAXA
		 */
		public static final String SUBTAXA = "subtaxa";

	}

	public static interface Collecting {

		/**
		 * Corresponds to COLLDATE
		 */
		public static final String COLLDATE = "collDate";

		/**
		 * Corresponds to COLLSITE
		 */
		public static final String COLLSITE = "collSite";

		/**
		 * Corresponds to MCPD COLLNUMB
		 */
		public static final String COLLNUMB = "collNumb";

		/**
		 * Corresponds to MCPD COLLSRC
		 */
		public static final String COLLSRC = "collSrc";

		/**
		 * FAO WIEWS code of collecting institute
		 */
		public static final String COLLCODE = "collCode";

		/**
		 * The name of the collecting institute if collCode is not available.
		 */
		public static final String COLLNAME = "collName";

		/**
		 * The address of the collecting institute if collCode is not available.
		 */
		public static final String COLLINSTADDRESS = "collInstAddress";

		/**
		 * Corresponds to COLLMISSID
		 */
		public static final String COLLMISSID = "collMissId";
	}

	public static interface Geo {

		public static final String LATITUDE_LONGITUDE = "latitudeAndLongitude";
		/**
		 * Latitude
		 */
		public static final String LATITUDE = "latitude";

		/**
		 * Longitude
		 */
		public static final String LONGITUDE = "longitude";

		/**
		 * Elevation
		 */
		public static final String ELEVATION = "elevation";

		/**
		 * COORDUNCERT (in meters)
		 */
		public static final String COORDUNCERT = "coordUncert";

		/**
		 * Corresponds to COORDATUM
		 */
		public static final String COORDDATUM = "coordDatum";

		/**
		 * Corresponds to GEOREFMETH
		 */
		public static final String GEOREFMETH = "georefMeth";
	}
}
