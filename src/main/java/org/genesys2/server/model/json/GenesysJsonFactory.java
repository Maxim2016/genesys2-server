/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.Taxonomy2;

public class GenesysJsonFactory {

	public static AccessionJson from(AccessionData accession) {
		if (accession == null) {
			return null;
		}

		final AccessionJson aj = new AccessionJson();
		aj.setVersion(accession.getVersion());
		aj.setGenesysId(accession.getAccessionId().getId());

		aj.setInstCode(accession.getInstitute().getCode());
		aj.setAcceNumb(accession.getAccessionName());
		aj.setUuid(accession.getUuid());

		final Taxonomy2 tax = accession.getTaxonomy();
		if (tax != null) {
			aj.setGenus(tax.getGenus());
			aj.setSpecies(tax.getSpecies());
			aj.setSpauthor(tax.getSpAuthor());
			aj.setSubtaxa(tax.getSubtaxa());
			aj.setSubtauthor(tax.getSubtAuthor());
		}
		if (accession.getCountryOfOrigin() != null) {
			aj.setOrgCty(accession.getCountryOfOrigin().getCode3());
		}

		aj.setAcqDate(accession.getAcquisitionDate());
		aj.setMlsStat(accession.getMlsStatus());
		aj.setInTrust(accession.getInTrust());
		aj.setAvailable(accession.getAvailability());
		aj.setHistoric(accession.getHistoric());
		// private Integer[] storage;
		aj.setStorage(accession.getStoRage().toArray(ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY));
		// private Integer sampStat;
		aj.setSampStat(accession.getSampleStatus());
		// private String[] duplSite;
		aj.setDuplSite(toStrArr(accession.getDuplSite()));

		// private String bredCode;
		// private String ancest;
		// private String donorCode;
		// private String donorNumb;
		// private String donorName;
		// private CollectingJson coll;
		// private GeoJson geo;

		return aj;
	}

	private static String[] toStrArr(String arrString) {
		if (StringUtils.isBlank(arrString)) {
			return null;
		}
		return arrString.split("\\s*;\\s*");
	}

	public static GeoJson from(AccessionGeo geo) {
		if (geo == null) {
			return null;
		}

		final GeoJson gj = new GeoJson();
		gj.setCoordDatum(geo.getDatum());
		gj.setCoordUncert(geo.getUncertainty());
		gj.setElevation(geo.getElevation());
		gj.setGeorefMeth(geo.getMethod());
		gj.setLatitude(geo.getLatitude());
		gj.setLongitude(geo.getLongitude());
		return gj;
	}

	public static CollectingJson from(AccessionCollect collect) {
		if (collect == null) {
			return null;
		}

		final CollectingJson col = new CollectingJson();
		col.setCollCode(collect.getCollCode());
		col.setCollDate(collect.getCollDate());
		col.setCollInstAddress(collect.getCollInstAddress());
		col.setCollMissId(collect.getCollMissId());
		col.setCollName(collect.getCollName());
		col.setCollNumb(collect.getCollNumb());
		col.setCollSite(collect.getCollSite());
		col.setCollSrc(collect.getCollSrc());
		return col;
	}

	public static Remark[] from(List<AccessionRemark> listAccessionRemarks) {
		if (listAccessionRemarks == null)
			return null;
		Remark[] rs = new Remark[listAccessionRemarks.size()];
		for (int i = 0; i < listAccessionRemarks.size(); i++) {
			rs[i] = from(listAccessionRemarks.get(i));
		}
		return rs;
	}

	private static Remark from(AccessionRemark accessionRemark) {
		Remark r = new Remark();
		r.setFieldName(accessionRemark.getFieldName());
		r.setRemark(accessionRemark.getRemark());
		return r;
	}

	public static void addBreeding(final AccessionJson aj, final AccessionBreeding breeding) {
		if (breeding != null) {
			aj.setBredCode(breeding.getBreederCode());
			aj.setAncest(breeding.getPedigree());
		}
	}

	public static void addCollecting(final AccessionJson aj, final AccessionCollect collect) {
		aj.setColl(from(collect));
	}

	public static void addGeo(final AccessionJson aj, final AccessionGeo geo) {
		aj.setGeo(from(geo));
	}

	public static void addRemarks(final AccessionJson aj, List<AccessionRemark> remarks) {
		aj.setRemarks(from(remarks));
	}

}
