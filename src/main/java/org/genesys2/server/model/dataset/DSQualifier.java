package org.genesys2.server.model.dataset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.impl.Descriptor;

/**
 * Dataset qualifier specification
 */
@Entity
@Table(name = "ds2qualifier")
public class DSQualifier extends BusinessModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9138418083256938915L;

	@Column(name="idx")
	private float index = 1.0f;

	@ManyToOne(optional = false)
	@JoinColumn(name="ds")
	private DS dataset;

	@ManyToOne(optional = false)
	@JoinColumn(name="d")
	private Descriptor descriptor;

	public float getIndex() {
		return index;
	}

	public void setIndex(float index) {
		this.index = index;
	}

	public DS getDataset() {
		return dataset;
	}

	public void setDataset(DS dataset) {
		this.dataset = dataset;
	}

	public Descriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(Descriptor descriptor) {
		this.descriptor = descriptor;
	}
	
	@Override
	public String toString() {
		return "DSQ " + this.id + " [" + this.getId() + "]";
	}
}
