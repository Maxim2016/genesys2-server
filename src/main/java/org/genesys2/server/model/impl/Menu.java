/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;

/**
 * CMS Menu.
 */
@Entity
@Table(name = "cmsmenu")
public class Menu extends BusinessModel {

	private static final long serialVersionUID = 8077072929931470029L;

	@Column(name = "menukey", length = 30, unique = true)
	private String key;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "menu")
	@OrderBy("orderIndex")
	private List<MenuItem> items = new ArrayList<MenuItem>();

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<MenuItem> getItems() {
		return items;
	}

	public void setItems(List<MenuItem> items) {
		this.items = items;
	}
}
