/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.model.genesys.AccessionId;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Entity
@Table(name = "accelist")
public class AccessionList extends VersionedAuditedModel implements AclAwareModel {
	private static final long serialVersionUID = 991886970995006680L;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	@Column(name = "uuid", unique = true, nullable = false, updatable = false, columnDefinition = "binary(16)")
	protected UUID uuid;

	@ManyToMany(cascade = {}, fetch = FetchType.LAZY)
	@JoinTable(name = "accelistitems", joinColumns = @JoinColumn(name = "listid") , inverseJoinColumns = @JoinColumn(name = "acceid") )
	private Set<AccessionId> accessionIds;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description", nullable = true)
	private String description;

	@Column
	private boolean shared;

	public UUID getUuid() {
		return uuid;
	}

	protected void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	@PrePersist
	protected void pre() {
		if (uuid == null)
			uuid = UUID.randomUUID();
	}

	public AccessionList() {
		super();
	}

	protected Set<AccessionId> getAccessionIds() {
		return accessionIds;
	}

	protected void setAccessionIds(Set<AccessionId> accessionIds) {
		this.accessionIds = accessionIds;
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public boolean isShared() {
		return shared;
	}
	
	public boolean getShared() {
		return shared;
	}

}