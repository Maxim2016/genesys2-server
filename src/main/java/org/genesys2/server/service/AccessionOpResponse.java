/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.Serializable;
import java.util.UUID;

/**
 * Upsert response
 */
public class AccessionOpResponse implements Serializable {
	private static final long serialVersionUID = 3266359129838078166L;

	private String instCode;
	private String acceNumb;
	private String genus;
	private String error;
	private UpsertResult result;

	public static class UpsertResult {
		private Type action = Type.NOOP;
		private UUID uuid;

		public static enum Type {
			NOOP, INSERT, UPDATE, DELETE
		}

		/**
		 * @param action
		 *            UPSERT or INSERT or DELETE
		 */
		public UpsertResult(Type action) {
			this.action = action;
		}

		public Type getAction() {
			return action;
		}

		public void setAction(Type action) {
			this.action = action;
		}

		public UUID getUuid() {
			return uuid;
		}

		public void setUUID(UUID uuid) {
			this.uuid = uuid;
		}
	}

	public AccessionOpResponse(String instCode, String acceNumb, String genus) {
		this.instCode = instCode;
		this.acceNumb = acceNumb;
		this.genus = genus;
	}

	public String getInstCode() {
		return instCode;
	}

	public String getAcceNumb() {
		return acceNumb;
	}

	public String getGenus() {
		return genus;
	}

	public String getError() {
		return error;
	}

	public UpsertResult getResult() {
		return result;
	}

	public void setResult(UpsertResult result) {
		this.result = result;
	}

	public void setError(String error) {
		this.error = error;
	}
}
