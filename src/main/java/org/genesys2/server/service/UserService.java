/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;

import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.wrapper.UserWrapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {

	List<UserRole> listAvailableRoles();

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	void addUser(User user) throws UserException;

	User createAccount(String email, String initialPassword, String fullName);

	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#user, 'WRITE')")
	void updateUser(User user) throws UserException;

	// @PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.id == #userId")
	void updatePassword(long userId, String rawPassword) throws UserException;

	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#user, 'WRITE')")
	void removeUser(User user) throws UserException;

	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#user, 'WRITE')")
	void removeUserById(long userId) throws UserException;

	@PreAuthorize("isAuthenticated()")
	User getMe();

	User getUserByEmail(String email);

	User getUserByUuid(String uuid);

	User getUserById(long userId) throws UserException;

	boolean exists(String username);

	Page<UserWrapper> listWrapped(int startRow, int pageSize) throws UserException;

	UserWrapper getWrappedById(long userId) throws UserException;

	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.id == #userId")
	User updateData(long userId, String name, String email) throws UserException;

	User getSystemUser(String string);

	Page<User> listUsers(Pageable pageable);

	void setAccountLock(String uuid, boolean locked);

	void setAccountEnabled(String uuid, boolean enabled);

	void setAccountLockLocal(String uuid, boolean locked);

	void userEmailValidated(String uuid);

	void addVettedUserRole(String uuid);

	void updateRoles(User user, List<String> selectedRoles);

	UserDetails getUserDetails(String userUuid);

	UserDetails getUserDetails(User user);

	List<User> autocompleteUser(String email);

}
