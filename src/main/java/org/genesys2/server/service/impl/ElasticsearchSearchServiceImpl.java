package org.genesys2.server.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder.Operator;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MaxValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MinValueFilter;
import org.genesys2.server.service.impl.FilterHandler.StartsWithFilter;
import org.genesys2.server.service.impl.FilterHandler.ValueRangeFilter;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.util.NumberUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.ElasticsearchException;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.FacetRequest;
import org.springframework.data.elasticsearch.core.facet.request.TermFacetRequestBuilder;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQueryBuilder;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import com.hazelcast.core.ILock;

@Service
public class ElasticsearchSearchServiceImpl implements ElasticService, InitializingBean {

	private static final Log LOG = LogFactory.getLog(ElasticsearchSearchServiceImpl.class);

	private static final String INDEXALIAS_PASSPORT_READ = "passport";
	private static final String INDEXALIAS_PASSPORT_WRITE = "passportWrite";

	@Autowired
	private Client client;

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private FilterHandler filterHandler;

	@Autowired
	private ElasticUpdater elasticUpdater;

	@Resource
	private ILock elasticsearchAdminLock;

	private final Map<String, Class<?>> clazzMap;

	@Autowired
	@Qualifier("genesysLowlevelRepositoryCustomImpl")
	private GenesysLowlevelRepository genesysLowlevelRepository;

	public ElasticsearchSearchServiceImpl() {
		clazzMap = new HashMap<String, Class<?>>();
		clazzMap.put(Accession.class.getName(), AccessionDetails.class);
	}

	@Override
	public Page<AccessionDetails> search(String query, Pageable pageable) throws SearchException {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ).withTypes("mcpd")
				.withQuery(org.elasticsearch.index.query.QueryBuilders.queryString(query).defaultOperator(Operator.AND)).withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public List<String> autocompleteSearch(String query) throws SearchException {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ).withTypes("mcpd")
				.withQuery(org.elasticsearch.index.query.QueryBuilders.queryString("acceNumb:(" + query + "*)").defaultOperator(Operator.AND))
				.withSort(SortBuilders.fieldSort(FilterConstants.ACCENUMB).order(SortOrder.ASC)).withPageable(new PageRequest(0, 10)).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			List<String> sugg = new ArrayList<String>(10);
			for (AccessionDetails ad : sampleEntities) {
				sugg.add(ad.getAcceNumb() + ", " + ad.getTaxonomy().getGenus() + ", " + ad.getInstitute().getCode());
			}
			return sugg;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public Page<AccessionDetails> filter(AppliedFilters appliedFilters, Pageable pageable) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		SortBuilder sortBuilder = SortBuilders.fieldSort(FilterConstants.ACCENUMB).order(SortOrder.ASC);
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ).withTypes("mcpd")
				.withFilter(filterBuilder).withSort(sortBuilder).withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public TermResult termStatistics(AppliedFilters appliedFilters, String term, int size) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		FacetRequest termFacetRequest;
		if (FilterConstants.DUPLSITE.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).excludeTerms("NOR051").size(size).build();
		} else if (FilterConstants.SGSV.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(FilterConstants.IN_SGSV).size(size).build();
		} else {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).size(size).build();
		}
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ).withTypes("mcpd")
				.withFilter(filterBuilder).withFacet(termFacetRequest).build();

		try {
			FacetedPage<AccessionDetails> page = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return (TermResult) page.getFacet("f");

		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	/**
	 * Runs TermFacet, but will automatically increase size if #otherCount is
	 * more than 10%
	 */
	@Override
	public TermResult termStatisticsAuto(AppliedFilters appliedFilters, String term, int size) throws SearchException {
		TermResult termResult = null;
		int newSize = size;
		do {
			termResult = termStatistics(appliedFilters, term, newSize);

			// Avoid div/0
			if (termResult.getTotalCount() == 0)
				break;

			double otherPerc = (double) termResult.getOtherCount() / (termResult.getMissingCount() + termResult.getTotalCount());
			if (otherPerc < 0.1)
				break;
			newSize += size + Math.max(1, (size / 3));
		} while (newSize < 2 * size);
		return termResult;
	}

	private AndFilterBuilder getFilterBuilder(AppliedFilters appliedFilters) {
		AndFilterBuilder filterBuilder = FilterBuilders.andFilter();

		for (AppliedFilter appliedFilter : appliedFilters) {
			applyFilter(filterBuilder, appliedFilter);
		}

		{
			// Handle HISTORIC: When filter not provided by user, apply
			// historic=false
			if (!appliedFilters.hasFilter(FilterConstants.HISTORIC)) {
				applyFilter(filterBuilder, FilterHandler.NON_HISTORIC_FILTER);
			}
		}

		return filterBuilder;
	}

	private void applyFilter(AndFilterBuilder filterBuilder, AppliedFilter appliedFilter) {
		String key = appliedFilter.getFilterName();

		GenesysFilter genesysFilter = filterHandler.getFilter(key);

		if (genesysFilter == null) {
			LOG.warn("No such filter " + key);
			return;
		}

		// Filter-level OR
		OrFilterBuilder orFilter = FilterBuilders.orFilter();

		// null
		if (appliedFilter.getWithNull()) {
			orFilter.add(FilterBuilders.missingFilter(key));
		}

		Set<FilterValue> filterValues = appliedFilter.getValues();
		if (filterValues != null && !filterValues.isEmpty()) {

			{
				// Handle literals
				Set<Object> literals = new HashSet<Object>();
				for (FilterValue filterValue : filterValues) {
					if (filterValue instanceof FilterHandler.LiteralValueFilter) {
						FilterHandler.LiteralValueFilter literal = (LiteralValueFilter) filterValue;
						literals.add(literal.getValue());
					}
				}

				if (!literals.isEmpty()) {

					if (genesysFilter.isAnalyzed()) {
						// query
						StringBuilder sb = new StringBuilder();
						for (Object val : literals) {
							if (sb.length() > 0)
								sb.append(",");
							if (val instanceof String)
								sb.append("\"" + val + "\"");
							else
								sb.append(val);
						}

						if (FilterConstants.ALIAS.equals(key)) {
							// Nested
							orFilter.add(FilterBuilders.nestedFilter("aliases", QueryBuilders.queryString("aliases.name" + ":(" + sb.toString() + ")")));
						} else {
							orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryString(key + ":(" + sb.toString() + ")")));
						}
					} else {
						// terms

						if (FilterConstants.SGSV.equals(key)) {
							orFilter.add(FilterBuilders.termsFilter(FilterConstants.IN_SGSV, literals).execution("or"));
						} else {
							orFilter.add(FilterBuilders.termsFilter(key, literals).execution("or"));
						}
					}
				}
			}

			{
				// Handle operations
				for (FilterValue filterValue : filterValues) {
					if (filterValue instanceof ValueRangeFilter) {
						ValueRangeFilter range = (ValueRangeFilter) filterValue;
						LOG.debug("Range " + range.getClass() + " " + range);
						orFilter.add(FilterBuilders.rangeFilter(key).from(range.getFrom()).to(range.getTo()));
					} else if (filterValue instanceof MaxValueFilter) {
						MaxValueFilter max = (MaxValueFilter) filterValue;
						LOG.debug("Max " + max);
						orFilter.add(FilterBuilders.rangeFilter(key).to(max.getTo()));
					} else if (filterValue instanceof MinValueFilter) {
						MinValueFilter min = (MinValueFilter) filterValue;
						LOG.debug("Min " + min);
						orFilter.add(FilterBuilders.rangeFilter(key).from(min.getFrom()));
					} else if (filterValue instanceof StartsWithFilter) {
						StartsWithFilter startsWith = (StartsWithFilter) filterValue;
						LOG.debug("startsWith " + startsWith);
						if (genesysFilter.isAnalyzed()) {
							if (FilterConstants.ALIAS.equals(key)) {
								orFilter.add(FilterBuilders.nestedFilter("aliases",
										QueryBuilders.queryString("aliases.name" + ":" + startsWith.getStartsWith() + "*")));
							} else {
								orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryString(key + ":" + startsWith.getStartsWith() + "*")));
							}
						} else {
							orFilter.add(FilterBuilders.prefixFilter(key, startsWith.getStartsWith()));
						}
					}
				}
			}
		}

		if (appliedFilter.isInverse()) {
			filterBuilder.add(FilterBuilders.notFilter(orFilter));
		} else {
			filterBuilder.add(orFilter);
		}

	}

	@Override
	public void updateAll(String className, Collection<Long> ids) {
		if (!clazzMap.containsKey(className)) {
			return;
		}

		if (ids.isEmpty()) {
			LOG.info("Skipping empty updateAll.");
			return;
		}

		if (LOG.isDebugEnabled())
			LOG.debug("Updating " + className + " bulk_size=" + ids.size());

		List<IndexQuery> queries = new ArrayList<IndexQuery>();

		List<AccessionDetails> ads = genesysService.getAccessionDetails(ids);

		for (AccessionDetails ad : ads) {
			if (ad == null)
				continue; // Skip null id

			IndexQuery iq = new IndexQuery();
			iq.setIndexName(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE);
			iq.setType("mcpd");
			iq.setId(String.valueOf(ad.getId()));
			iq.setObject(ad);

			queries.add(iq);
		}

		if (!queries.isEmpty()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing " + className + " count=" + queries.size() + " of provided objects count=" + ids.size());
			}
			elasticsearchTemplate.bulkIndex(queries);
		}
	}

	@Override
	public void remove(String className, long id) {
		Class<?> clazz2 = clazzMap.get(className);
		if (clazz2 == null) {

			return;
		}
		LOG.info("Removing from index " + clazz2 + " id=" + id);

		if (clazz2.equals(AccessionDetails.class)) {
			elasticsearchTemplate.delete(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE, "mcpd", String.valueOf(id));
		} else {
			// Default
			elasticsearchTemplate.delete(clazz2, String.valueOf(id));
		}
	}

	@Override
	public void refreshIndex(String className) {
		Class<?> clazz2 = clazzMap.get(className);
		if (clazz2 == null) {

			return;
		}
		LOG.info("Refreshing index " + clazz2);
		elasticsearchTemplate.refresh(clazz2, true);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOG.info("Initializing index");
		elasticsearchTemplate.createIndex(AccessionDetails.class);
		LOG.info("Putting mapping");
		try {
			elasticsearchTemplate.putMapping(AccessionDetails.class);
		} catch (Throwable e) {
			LOG.error("Mapping mismatch. Need to reindex.");
		}
		LOG.info("Refreshing");
		elasticsearchTemplate.refresh(AccessionDetails.class, true);

		Map<?, ?> indexMapping = elasticsearchTemplate.getMapping(AccessionDetails.class);

		// Ensure ES index 'genesysarchive'
		if (!elasticsearchTemplate.indexExists("genesysarchive")) {
			LOG.info("Initializing genesysarchive");
			elasticsearchTemplate.createIndex("genesysarchive");
			LOG.info("Copying mapping to genesysarchive");
			elasticsearchTemplate.putMapping("genesysarchive", "mcpd", indexMapping);
		}

		if (!aliasExists(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ)) {
			realias(INDEXALIAS_PASSPORT_READ, "genesys");
		}
		if (!aliasExists(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE)) {
			realias(INDEXALIAS_PASSPORT_WRITE, "genesys");
		}
	}

	/**
	 * Check if ES alias exists and points to some index
	 * 
	 * @param aliasName
	 * @return true if alias exists
	 */
	private boolean aliasExists(String aliasName) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Checking for ES alias " + aliasName + " on client=" + client);
		}
		ImmutableOpenMap<String, AliasMetaData> x = client.admin().cluster().prepareState().execute().actionGet().getState().getMetaData().getAliases()
				.get(aliasName);

		if (x == null) {
			LOG.debug("Alias does not exist");
			return false;
		} else {
			LOG.debug("Got something back " + x);
			return x.keysIt().hasNext();
		}
	}

	/**
	 * Create new ES indexes
	 */
	@Override
	public void regenerateIndexes() throws ElasticsearchException {
		regeneratePassportIndex();
	}

	/**
	 * Create a new passport data index based on current timestamp, reindex it,
	 * change alias to point to index
	 */
	private void regeneratePassportIndex() throws ElasticsearchException {
		try {
			if (elasticsearchAdminLock.tryLock(10, TimeUnit.SECONDS)) {
				long time = System.currentTimeMillis();
				String passportIndexName = ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ + time;

				Map<?, ?> indexMapping = elasticsearchTemplate.getMapping(AccessionDetails.class);
				Map<?, ?> settings = elasticsearchTemplate.getSetting(AccessionDetails.class);

				if (elasticsearchTemplate.indexExists(passportIndexName)) {
					throw new ElasticsearchException("Index already exists with name " + passportIndexName);
				}

				createIndex(passportIndexName, indexMapping, settings);
				realias(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE, passportIndexName);
				reindex(new AppliedFilters());
				realias(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_READ, passportIndexName);

			} else {
				throw new ElasticsearchException("Could not acquire elasticsearchAdminLock lock");
			}
		} catch (InterruptedException e) {
		} finally {
			elasticsearchAdminLock.unlock();
		}
	}

	/**
	 * Make the alias point exclusively to the specified index
	 * 
	 * @param aliasName
	 *            The alias name
	 * @param indexName
	 *            The index the alias points to
	 */
	@Override
	public void realias(String aliasName, String indexName) {

		if (LOG.isDebugEnabled())
			LOG.debug("Loading alias definition for " + aliasName);

		deleteAlias(aliasName);

		AliasQuery query = new AliasQuery();
		query.setAliasName(aliasName);
		query.setIndexName(indexName);
		LOG.info("Adding alias " + aliasName + " to index " + indexName);
		elasticsearchTemplate.addAlias(query);
	}

	@Override
	public void deleteAlias(String aliasName) {
		ImmutableOpenMap<String, AliasMetaData> x = client.admin().cluster().prepareState().execute().actionGet().getState().getMetaData().getAliases()
				.get(aliasName);

		if (x != null) {
			final Set<String> allIndices = new HashSet<>();
			x.keysIt().forEachRemaining(allIndices::add);

			for (String aliasIndex : allIndices) {
				AliasQuery query = new AliasQuery();
				query.setAliasName(aliasName);
				query.setIndexName(aliasIndex);

				LOG.info("Removing alias " + aliasName + " from index " + aliasIndex);
				elasticsearchTemplate.removeAlias(query);
			}
		}
	}

	private void createIndex(String indexName, Map<?, ?> indexMapping, Map<?, ?> settings) {
		LOG.info("Creating index " + indexName);
		elasticsearchTemplate.createIndex(indexName, settings);
		LOG.info("Copying mapping to genesysarchive");
		elasticsearchTemplate.putMapping(indexName, "mcpd", indexMapping);
	}

	@Override
	public void regenerateAccessionSequentialNumber() {
		final List<Object[]> batch = new ArrayList<Object[]>();

		genesysLowlevelRepository.listAccessions(null, new RowCallbackHandler() {
			private int counter = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				long acceId = rs.getLong(1);
				String acceNumb = rs.getString(4);
				counter++;

				Object[] o = new Object[] { acceId, acceNumb, NumberUtils.numericValue(acceNumb) };
				batch.add(o);

				if (batch.size() >= 100) {
					if (counter % 100000 == 0)
						LOG.info("Regenerating sequential numbers " + counter);
					else
						LOG.debug("Regenerating sequential numbers " + counter);

					updateAccessionSequentialNumber(batch);
					batch.clear();
				}
			}
		});

		updateAccessionSequentialNumber(batch);
	}

	/**
	 * Data[] = accessionId, acceNumb and seqNo
	 * 
	 * @param batch
	 */
	private void updateAccessionSequentialNumber(List<Object[]> batch) {
		List<UpdateQuery> queries = new ArrayList<UpdateQuery>(batch.size());

		List<Long> missingDocIds = new ArrayList<Long>(batch.size());

		for (Object[] o : batch) {
			try {

				IndexRequest indexRequest = new IndexRequest();
				indexRequest.index(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE).type("mcpd");
				indexRequest.source("seqNo", o[2]);
				UpdateQuery updateQuery = new UpdateQueryBuilder().withClass(AccessionDetails.class)
						.withIndexName(ElasticsearchSearchServiceImpl.INDEXALIAS_PASSPORT_WRITE).withType("mcpd").withId(o[0].toString())
						.withIndexRequest(indexRequest).build();
				queries.add(updateQuery);
				// LOG.debug("ES added seqNo to " + o[0].toString());

			} catch (Throwable e) {
				// LOG.error(e.getMessage());
				missingDocIds.add((Long) o[0]);
			}
		}

		if (queries.size() > 0) {
			// LOG.info("Reindexing accession documents: " + queries.size());
			elasticsearchTemplate.bulkUpdate(queries);
		}

		if (missingDocIds.size() > 0) {
			// LOG.info("Reindexing accession documents: " +
			// missingDocIds.size());
			updateAll(Accession.class.getName(), missingDocIds);
		}
	}

	@Override
	public void reindex(AppliedFilters filters) {
		final List<Long> ids = new ArrayList<>(100);

		genesysLowlevelRepository.listAccessionIds(filters, null, new RowCallbackHandler() {
			private long count = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				count++;
				add(rs.getLong(1));
			}

			private void add(long accessionId) {
				if (ids.size() >= 100) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Scheduling reindex of batch " + (count / 100) + " size=" + ids.size());
					}
					elasticUpdater.updateAll(Accession.class, ids.toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
					ids.clear();
				}
				ids.add(accessionId);
			}
		});

		if (ids.size() >= 100) {
			// Final kick
			elasticUpdater.updateAll(Accession.class, ids.toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
		}
		LOG.info("Done.");
	}

}
