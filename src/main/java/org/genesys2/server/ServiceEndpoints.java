/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server;

public interface ServiceEndpoints {

	/* =============================================================== */
	/* ==================== Service endpoint /me ===================== */
	/* =============================================================== */

	// Retrieve current User profile
	String ME = "/me";

	// List Organizations that current User belongs to
	String MY_ORGANIZATIONS = "/me/organizations";

	// List DataSets owned by current User
	String MY_DATA_SETS = "/me/datasets";

	// List Traits owned by current User
	String MY_TRAITS = "/me/traits";

	/* =============================================================== */
	/* =============== Service endpoint /organizations =============== */
	/* =============================================================== */

	// Lists all Traits associated with Organization $O
	String ORGANIZATION_TRAITS = "/organizations/{organizationId}/traits";

	// Lists all DataSets associated with Organization $O
	String ORGANIZATION_DATA_SETS = "/organizations/{organizationId}/datasets";

	// List all Licenses associated with Organization $O
	String ORGANIZATION_LICENSES = "/organizations/{organizationId}/licenses";

	// Issue a request for current User to join Organization $O
	String JOIN_ORGANIZATION = "/organizations/{organizationId}/join";

	/* =============================================================== */
	/* =============== Service endpoint /traits ====================== */
	/* =============================================================== */

	// Lists all Traits available to Current User
	String TRAITS = "/traits";

	// Remove Trait
	String REMOVE_TRAIT = "/traits/{traitId}";

	/* =============================================================== */
	/* =============== Service endpoint /licenses ==================== */
	/* =============================================================== */

	// Lists all Licenses available to Current User
	String LICENSES = "/licenses";

	// Remove License {licenseId}
	String REMOVE_LICENSE = "/licenses/{licenseId}";

	/* =============================================================== */
	/* =============== Service endpoint /datasets ==================== */
	/* =============================================================== */

	// List available DataSets
	String DATASETS = "/datasets";

	// Remove data set {dataSetId}
	String REMOVE_DATASET = "/datasets/{dataSetId}";

	// Get all Entries of DataSet {dataSetId}
	String DATASET_ENTRIES = "/datasets/{dataSetId}/data";

	// Update Entry {dataEntryId} in DataSet {dataSetId}
	String UPDATE_DATASET_ENTRY = "/datasets/{dataSetId}/data/{dataEntryId}";

	// Delete Entry {dataEntryId} from DataSet {dataSetId}
	String REMOVE_DATASET_ENTRY = "/datasets/{dataSetId}/data/{dataEntryId}";

	/* =============================================================== */
	/* =============== Service endpoint /tokens ==================== */
	/* =============================================================== */

	// List available Tokens
	String LIST_USER_TOKENS = "/users/{username}/list/tokens";

	String REVOKE_USER_TOKEN = "/users/{username}/tokens/revoke/{token}";

	String LIST_CLIENT_TOKEN = "/clients/{client}/list/tokens";

	String REVOKE_CLIENT_TOKEN = "/clients/{client}/tokens/revoke/{token}";

}
