/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import java.io.IOException;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component("webApiFilter")
public class WebApiFilter extends OncePerRequestFilter {
	private static final Log LOG = LogFactory.getLog(WebApiFilter.class);

	@Autowired
	private OAuth2ClientDetailsService clientDetailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String clientId = request.getParameter("client_id");
		String clientSecret = request.getParameter("client_secret");
		String referrer = request.getHeader("Referer");

		try {
			if (StringUtils.isBlank(clientId)) {
				throw new Exception("client_id not provided");
			}

			ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);

			if (clientDetails == null) {
				throw new Exception("Invalid client_id, client_secret combination");
			}

			if (StringUtils.isNotBlank(clientDetails.getClientSecret()) && !clientDetails.getClientSecret().equals(clientSecret)) {
				throw new Exception("Invalid client secret");
			}
			if (StringUtils.isBlank(referrer)) {
				throw new Exception("Referrer not provided by client");
			}

			if (!isRegisteredReferrer(referrer, clientDetails.getRegisteredRedirectUri())) {
				throw new Exception("Referrer not registered with client " + referrer);
			}

			Authentication webapiClient = new PreAuthenticatedAuthenticationToken(clientDetails.getClientId(), null, clientDetails.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(webapiClient);
			try {
				filterChain.doFilter(request, response);
			} finally {
				SecurityContextHolder.getContext().setAuthentication(null);
			}

		} catch (Throwable e) {
			LOG.warn(e.getMessage());
			response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
		}

	}

	private boolean isRegisteredReferrer(String referrer, Set<String> baseUrlSet) {
		for (String baseUrl : baseUrlSet) {
			if (referrer.startsWith(baseUrl)) {
				return true;
			}
		}
		return false;
	}
}
