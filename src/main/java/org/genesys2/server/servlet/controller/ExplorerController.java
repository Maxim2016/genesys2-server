/**
 * Copyright 2015 Global Crop Diversity Trust
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DownloadService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.MappingService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UrlShortenerService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.GenesysFilterServiceImpl.LabelValue;
import org.genesys2.server.service.impl.SearchException;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhlabs.image.MapColorsFilter;

@Controller
public class ExplorerController extends BaseController implements InitializingBean {

	private static final int DOWNLOAD_LIMIT = 200000;

	@Autowired
	private GenesysFilterService filterService;

	@Autowired
	private ElasticService elasticService;

	@Autowired
	private CropService cropService;

	@Autowired
	private TraitService traitService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private MappingService mappingService;

	@Autowired
	private FilterHandler filterHandler;

	@Autowired
	private DownloadService downloadService;

	@Autowired
	private UrlShortenerService urlShortenerService;

	private final ObjectMapper mapper = new ObjectMapper();

	@Value("${base.url}")
	private String baseUrl;

	private List<String> validDisplayColumns;

	private List<String> defaultDisplayColumns;

	@Override
	public void afterPropertiesSet() throws Exception {
		validDisplayColumns = genesysService.columnsAvailableForDisplay();
		defaultDisplayColumns = genesysService.defaultDisplayColumns();
	}

	/**
	 * Redirect to /explore/c/{shortName} if parameter 'crop' is provided
	 */
	@RequestMapping(value = "/explore", params = { "crop" })
	private String x(ModelMap model, @RequestParam(value = "filter", required = true, defaultValue = "{}") String jsonFilter,
			@RequestParam(value = "crop") String shortName) {

		model.addAttribute("filter", jsonFilter);

		return "redirect:/explore";
	}

	/**
	 * Explore accessions filtered within a crop
	 */
	@RequestMapping("/explore/c/{crop}")
	public String viewFiltered(ModelMap model, @PathVariable("crop") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException("No crop " + shortName);
		}

		model.addAttribute("filter", "{\"" + FilterConstants.CROPS + "\":[\"" + crop.getShortName() + "\"]}");
		model.addAttribute("page", page);
		return "redirect:/explore";
	}

	/**
	 * Browse all
	 *
	 * @param model
	 * @param page
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/explore")
	public String viewFiltered(HttpServletResponse response, ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "filter", required = true, defaultValue = "{}") String jsonFilter,
			@RequestParam(value = "columns", required = true, defaultValue = "") String[] columns,
			@CookieValue(value = "columns", required=false) String[] cookieColumns) throws IOException {

		String[] selectedFilters = null;

		_logger.debug("Filtering by: " + jsonFilter);
		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

		Crop crop = null;
		{
			String shortName = appliedFilters.getFirstLiteralValue(FilterConstants.CROPS, String.class);
			if (shortName != null)
				crop = cropService.getCrop((String) shortName);

			if (crop != null) {
				// Keep only one crop
				AppliedFilter af = appliedFilters.get(FilterConstants.CROPS);
				af.getValues().clear();
				af.addFilterValue(new FilterHandler.LiteralValueFilter(crop.getShortName()));
			}
		}
		model.addAttribute("crop", crop);

		// JSP works with JsonObject
		final Map<?, ?> filters = mapper.readValue(appliedFilters.toString(), Map.class);
		model.addAttribute("filters", filters);

		selectedFilters = appliedFilters.getFilterNames();
		final List<GenesysFilter> currentFilters = filterHandler.selectFilters(selectedFilters);
		final List<GenesysFilter> availableFilters = filterHandler.listAvailableFilters();

		if (_logger.isDebugEnabled()) {
			_logger.debug(appliedFilters.toString());
		}
		model.addAttribute("jsonFilter", appliedFilters.toString());

		final Page<AccessionDetails> accessions = filterService.listAccessionDetails(appliedFilters, new PageRequest(page - 1, 50, new Sort("acceNumb")));

		if (_logger.isDebugEnabled()) {
			_logger.debug("Got: " + accessions);
		}

		model.addAttribute("availableColumns", validDisplayColumns);
		model.addAttribute("selectedColumns", cleanupDisplayColumns(columns, cookieColumns, response));

		model.addAttribute("crops", cropService.list(getLocale()));
		model.addAttribute("pagedData", accessions);
		model.addAttribute("appliedFilters", appliedFilters);
		model.addAttribute("currentFilters", currentFilters);
		model.addAttribute("availableFilters", availableFilters);

		return "/accession/explore";
	}

	/**
	 * @param columns
	 *            list of explicitly selected columns
	 * @param cookieColumns
	 *            list of columns currently in the cookie
	 * @param response
	 *            HttpResponse to set valid cookieColumn value
	 * @return String[] of valid columns to display
	 */
	private List<String> cleanupDisplayColumns(String[] columns, String[] cookieColumns, HttpServletResponse response) {
		if (columns != null && columns.length > 0) {
			// Cleanup user input
			columns = keepValidColumns(columns, validDisplayColumns);
			// Update cookie accordingly
			return updateCookieColumns(columns, response);
		}

		if (cookieColumns != null && cookieColumns.length > 0) {
			String[] validCookieColumns = keepValidColumns(cookieColumns, validDisplayColumns);
			if (!Arrays.equals(cookieColumns, validCookieColumns)) {
				// Change detected, update cookie!
				return updateCookieColumns(cookieColumns, response);
			} else {
				return Arrays.asList(cookieColumns);
			}
		}

		// Return default columns
		return defaultDisplayColumns;
	}

	/**
	 * 
	 * @param cookieColumns
	 *            validated selected columns
	 * @param response
	 * @return
	 */
	private List<String> updateCookieColumns(String[] cookieColumns, HttpServletResponse response) {
		Cookie cookie = new Cookie("columns", "");

		// If cookieColumns match defaults, then delete the cookie
		if (Arrays.equals(cookieColumns, defaultDisplayColumns.toArray(ArrayUtils.EMPTY_STRING_ARRAY))) {
			_logger.debug("Selected view columns match defaults, removing cookie");
			// 0 = Delete, -1 = Keep Forever
			cookie.setMaxAge(0);
			response.addCookie(cookie);

			return defaultDisplayColumns;

		} else {
			StringBuffer cookieColumnsString = new StringBuffer();
			for (String cc : cookieColumns) {
				if (cookieColumnsString.length() > 0)
					cookieColumnsString.append(",");
				cookieColumnsString.append(cc);
			}

			cookie.setValue(cookieColumnsString.toString());
			cookie.setMaxAge(Integer.MAX_VALUE);
			response.addCookie(cookie);
			_logger.debug("Sending updated cookie: " + cookie.getValue());

			return Arrays.asList(cookieColumns);
		}
	}

	private String[] keepValidColumns(String[] columns, List<String> validColumns) {
		if (columns == null || columns.length == 0)
			return ArrayUtils.EMPTY_STRING_ARRAY;

		// Remove columns not listed in "available"
		List<String> list = new ArrayList<String>(Arrays.asList(columns));
		list.removeIf(a -> !validColumns.contains(a));

		return list.toArray(ArrayUtils.EMPTY_STRING_ARRAY);
	}

	/**
	 * Browse all using Elasticsearch
	 *
	 * @param model
	 * @param page
	 * @return
	 * @throws IOException
	 * @throws SearchException
	 */
	@RequestMapping("/explore-es")
	public String viewElasticFiltered(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "filter", required = true, defaultValue = "{}") String jsonFilter) throws IOException, SearchException {

		String[] selectedFilters = null;

		_logger.debug("Filtering by: " + jsonFilter);
		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

		Crop crop = null;
		{
			String shortName = appliedFilters.getFirstLiteralValue(FilterConstants.CROPS, String.class);
			if (shortName != null)
				crop = cropService.getCrop((String) shortName);

			if (crop != null) {
				// Keep only one crop
				AppliedFilter af = appliedFilters.get(FilterConstants.CROPS);
				af.getValues().clear();
				af.addFilterValue(new FilterHandler.LiteralValueFilter(crop.getShortName()));
			}
		}
		model.addAttribute("crop", crop);

		// JSP works with JsonObject // TODO Handle -filter.key!!
		final Map<?, ?> filters = mapper.readValue(appliedFilters.toString(), Map.class);
		model.addAttribute("filters", filters);

		selectedFilters = appliedFilters.getFilterNames();
		final List<GenesysFilter> currentFilters = filterHandler.selectFilters(selectedFilters);
		final List<GenesysFilter> availableFilters = filterHandler.listAvailableFilters();

		_logger.info(appliedFilters.toString());
		model.addAttribute("jsonFilter", appliedFilters.toString());

		final Page<AccessionDetails> accessions = elasticService.filter(appliedFilters, new PageRequest(page - 1, 50, new Sort("acceNumb")));

		_logger.info("Got: " + accessions);

		model.addAttribute("crops", cropService.list(getLocale()));
		model.addAttribute("pagedData", accessions);
		model.addAttribute("appliedFilters", appliedFilters);
		model.addAttribute("currentFilters", currentFilters);
		model.addAttribute("availableFilters", availableFilters);

		return "/accession/explore-es";
	}

	@RequestMapping(value = "/additional-filter", method = RequestMethod.GET)
	public String getAdditionalFilters(ModelMap model, @RequestParam(value = "filter", required = true, defaultValue = "") String[] selectedFilters)
			throws IOException {

		final List<GenesysFilter> additionalFilters = filterHandler.selectFilters(selectedFilters);
		model.addAttribute("additionalFilters", additionalFilters);

		if (ArrayUtils.contains(selectedFilters, "crops")) {
			_logger.debug("Adding crop list");
			model.addAttribute("crops", cropService.list(getLocale()));
		}

		return "/accession/additional-filter";
	}

	@RequestMapping(value = "/modal", method = RequestMethod.GET)
	public String getModelWindow(ModelMap model, @RequestParam(value = "shortName", required = true) String shortName) {

		final Crop crop = cropService.getCrop(shortName);
		if (crop != null) {

			final List<ParameterCategory> categories = traitService.listCategories();
			final Map<ParameterCategory, List<Parameter>> descriptors = traitService.mapTraits(crop, categories);
			final Map<Long, List<Method>> methods = traitService.mapMethods(crop);

			model.addAttribute("crop", crop);
			model.addAttribute("categories", categories);
			model.addAttribute("descriptors", descriptors);
			model.addAttribute("methods", methods);

		}

		return "/accession/modal";
	}

	@RequestMapping(value = "/explore/ac/{field:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<LabelValue<String>> autocomplete(@PathVariable("field") String filter, @RequestParam(value = "term", required = true) String ac,
			@RequestParam(value = "jsonFilter", required = false, defaultValue = "{}") String jsonFilter) throws IOException {

		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);
		return filterService.autocomplete(filter, ac, appliedFilters);
	}

	@RequestMapping(value = "/explore/map", method = RequestMethod.GET)
	public String map(ModelMap model, @RequestParam(value = "crop", required = false, defaultValue = "") String cropName,
			@RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter) throws IOException {

		Crop crop = null;
		if (StringUtils.isNotBlank(cropName)) {
			crop = cropService.getCrop(cropName);
			if (crop == null) {
				throw new ResourceNotFoundException("No crop " + cropName);
			}
			model.addAttribute("crop", crop);
		}
		final AppliedFilters appliedFilters = updateFilterWithCrop(cropName, jsonFilter);

		model.addAttribute("jsonFilter", appliedFilters.toString());
		return "/accession/map";
	}

	@RequestMapping(value = "/explore/dwca", method = RequestMethod.POST)
	public void dwca(ModelMap model, @RequestParam(value = "crop", required = false, defaultValue = "") String cropName,
			@RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter, HttpServletResponse response) throws IOException {

		final AppliedFilters appliedFilters = updateFilterWithCrop(cropName, jsonFilter);

		final int countFiltered = genesysService.countAccessions(appliedFilters);
		_logger.info("Attempting to download DwCA for " + countFiltered + " accessions");
		if (countFiltered > DOWNLOAD_LIMIT) {
			throw new RuntimeException("Refusing to export more than " + DOWNLOAD_LIMIT + " entries");
		}

		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-filtered.zip\""));

		// Write Darwin Core Archive to the stream.
		final OutputStream outputStream = response.getOutputStream();

		try {
			genesysService.writeAccessions(appliedFilters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted");
		}
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/explore/download/mcpd", method = RequestMethod.POST)
	public void downloadXlsxMCPD(ModelMap model, @RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter,
			HttpServletResponse response) throws IOException {

		final AppliedFilters appliedFilters = updateFilterWithCrop(null, jsonFilter);

		final int countFiltered = genesysService.countAccessions(appliedFilters);
		_logger.info("Attempting to download XLSX MCPD for " + countFiltered + " accessions");
		if (countFiltered > DOWNLOAD_LIMIT) {
			throw new RuntimeException("Refusing to export more than " + DOWNLOAD_LIMIT + " entries");
		}

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-filtered.xlsx\""));
		response.flushBuffer();

		// Write XLSX to the stream.
		final OutputStream outputStream = response.getOutputStream();

		try {
			downloadService.writeXlsxMCPD(appliedFilters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted", e);
		}
	}

	private AppliedFilters updateFilterWithCrop(String cropName, String jsonFilter) throws IOException {
		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

		Crop crop = null;
		if (StringUtils.isNotBlank(cropName)) {
			crop = cropService.getCrop(cropName);
			if (crop == null) {
				throw new ResourceNotFoundException("No crop " + cropName);
			} else {
				AppliedFilter cropFilter = appliedFilters.get(FilterConstants.CROPS);
				cropFilter.getValues().clear();
				cropFilter.addFilterValue(new FilterHandler.LiteralValueFilter(crop.getShortName()));
			}
		}

		return appliedFilters;
	}

	@RequestMapping(value = "/explore/kml", produces = "application/vnd.google-earth.kml+xml", method = RequestMethod.POST)
	@ResponseBody
	public String kml(@RequestParam(value = "crop", required = false, defaultValue = "") String cropName,
			@RequestParam(value = "filter", required = true) String jsonFilter, HttpServletResponse response) throws IOException {
		final AppliedFilters appliedFilters = updateFilterWithCrop(cropName, jsonFilter);

		response.setContentType("application/vnd.google-earth.kml+xml");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-kml-filtered.kml\""));

		return mappingService.filteredKml(appliedFilters);
	}

	/**
	 * Change color of the tile
	 *
	 * @param color
	 * @param imageBytes
	 * @return
	 */

	@RequestMapping(value = "/explore/geoJson", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String geoJson(@RequestParam(value = "crop", required = false, defaultValue = "") String cropName,
			@RequestParam(value = "limit", required = false, defaultValue = "") Integer limit,
			@RequestParam(value = "filter", required = true) String jsonFilter) throws IOException {

		final AppliedFilters appliedFilters = updateFilterWithCrop(cropName, jsonFilter);

		return mappingService.filteredGeoJson(appliedFilters, limit);
	}

	@RequestMapping(value = "/explore/tile/{zoom}/{x}/{y}", produces = MediaType.IMAGE_PNG_VALUE)
	public void tile(@PathVariable("zoom") int zoom, @PathVariable("x") int x, @PathVariable("y") int y,
			@RequestParam(value = "filter", required = true) String jsonFilter, @RequestParam(value = "color", required = false) String color,
			HttpServletResponse response) {

		try {
			AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

			byte[] image = mappingService.getTile(appliedFilters, zoom, x, y);
			image = changeColor(color, image);
			response.getOutputStream().write(image, 0, image.length);

		} catch (final IOException e) {
			_logger.warn(e.getMessage());
			throw new RuntimeException("Could not render image", e);
		} catch (final Throwable e) {
			_logger.error(e.getMessage(), e);
			throw new ResourceNotFoundException(e.getMessage());
		}
	}

	private byte[] changeColor(String color, byte[] imageBytes) {
		if (StringUtils.isBlank(color)) {
			return imageBytes;
		}

		if (!color.startsWith("#"))
			color = "#" + color;

		if (_logger.isDebugEnabled())
			_logger.debug("Changing color to " + color);

		try {
			final Color newColor = Color.decode(color);
			if (newColor.equals(MappingService.DEFAULT_TILE_COLOR)) {
				return imageBytes;
			}

			final int originalColor = MappingService.DEFAULT_TILE_COLOR.getRGB();
			final int updatedColor = newColor.getRGB();

			final MapColorsFilter mcf = new MapColorsFilter(originalColor, updatedColor);
			final ByteArrayInputStream bios = new ByteArrayInputStream(imageBytes);
			final BufferedImage image = mcf.filter(ImageIO.read(bios), null);

			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "PNG", baos);
			return baos.toByteArray();

		} catch (final NumberFormatException e) {
			_logger.warn("Cannot get color for " + color);
			return imageBytes;
		} catch (final IOException e) {
			_logger.warn(e.getMessage());
			return imageBytes;
		}
	}

	@RequestMapping(value = "/explore/overview")
	public String overview(ModelMap model, @RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter)
			throws IOException, SearchException {

		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);
		String[] selectedFilters = appliedFilters.getFilterNames();
		final List<GenesysFilter> currentFilters = filterHandler.selectFilters(selectedFilters);
		model.addAttribute("appliedFilters", appliedFilters);
		model.addAttribute("currentFilters", currentFilters);

		// JSP works with JsonObject
		final Map<?, ?> filters = mapper.readValue(appliedFilters.toString(), Map.class);
		model.addAttribute("filters", filters);
		model.addAttribute("jsonFilter", appliedFilters.toString());

		// Composition overview
		model.addAttribute("accessionCount", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTCODE, 10).getTotalCount());
		overviewInstitutes(model, appliedFilters);
		overviewComposition(model, appliedFilters);
		overviewAvailability(model, appliedFilters);
		overviewHistoric(model, appliedFilters);
		overviewManagement(model, appliedFilters);
		overviewSources(model, appliedFilters);

		return "/accession/overview";
	}

	private void overviewInstitutes(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsInstCode", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTCODE, 20));
		model.addAttribute("statsInstCountry", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTITUTE_COUNTRY_ISO3, 20));
	}

	private void overviewSources(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsOrgCty", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.ORGCTY_ISO3, 20));
		model.addAttribute("statsDonorCode", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.DONORCODE, 20));
	}

	private void overviewAvailability(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsMLS", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.MLSSTATUS, 2));
		model.addAttribute("statsAvailable", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.AVAILABLE, 2));
	}

	private void overviewHistoric(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsHistoric", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.HISTORIC, 2));
	}

	private void overviewManagement(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsStorage", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.STORAGE, 30));
		model.addAttribute("statsDuplSite", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.DUPLSITE, 20));
		model.addAttribute("statsSGSV", elasticService.termStatistics(appliedFilters, FilterConstants.SGSV, 2));
	}

	private void overviewComposition(ModelMap model, AppliedFilters appliedFilters) throws SearchException {
		model.addAttribute("statsGenus", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.TAXONOMY_GENUS, 20));
		model.addAttribute("statsSpecies", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.TAXONOMY_GENUSSPECIES, 20));
		model.addAttribute("statsCrops", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.CROPS, 30));
		model.addAttribute("statsSampStat", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.SAMPSTAT, 30));
	}

	@RequestMapping(value = "/explore/shorten-url", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getBriefURL(@RequestParam(value = "filter", required = false, defaultValue = "") String jsonFilter,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page, @CookieValue(required = false, value = "columns") String columns)
					throws IOException, URISyntaxException {

		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

		URIBuilder uriBuilder = new URIBuilder(baseUrl).setPath("/explore").addParameter("filter", appliedFilters.toString()).addParameter("page",
				Integer.toString(page));

		if (StringUtils.isNotBlank(columns)) {
			uriBuilder.addParameter("columns", columns);
		}

		URI longUrl = uriBuilder.build();
		final String shortenedUrl = urlShortenerService.shortenUrl(longUrl.toURL());

		return new Object() {
			@SuppressWarnings("unused")
			public String shortUrl = shortenedUrl;
		};
	}

}
