package org.genesys2.server.servlet.controller;

import org.apache.commons.io.IOUtils;
import org.genesys2.spring.ResourceNotFoundException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/doc")
public class AsciiDocController extends BaseController {

    private static final String DOCS_DIR = "/docs/";
    private static final String DOCS_SECTIONS_DIR = "sections/";
    private static final String DOCS_IMAGES_DIR = "images/";
    private static final String HTML_EXTENSION = ".html";
    private static final String PNG_EXTENSION = ".png";

    @RequestMapping("/0/{documentName}")
    public String viewDoc(ModelMap model, @PathVariable(value = "documentName") String documentName) throws IOException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        String docPath = new StringBuilder(DOCS_DIR).append(documentName)
                .append(HTML_EXTENSION).toString();

        InputStream resourceStream = classLoader.getResourceAsStream(docPath);
        if (resourceStream == null) {
            throw new ResourceNotFoundException();
        }

        String html = IOUtils.toString(resourceStream);

        Document document = Jsoup.parse(html);

        String title = document.getElementsByTag("title").text();
        String head = document.getElementsByTag("head").toString();
        String menu = document.getElementById("toc").toString();
        document.getElementById("toc").remove();

        Element header = document.getElementById("header");
        header.getElementsByTag("h1").remove();
        header.attr("id", "body-header");

        Element content = document.getElementById("content");
        content.attr("id", "body-content");

        Element footer = document.getElementById("footer");
        footer.attr("id", "body-footer");

        model.addAttribute("title", title);
        model.addAttribute("head", head);
        model.addAttribute("menu", menu);
        model.addAttribute("bodyHeader", header.toString());
        model.addAttribute("bodyContent", content.toString());
        model.addAttribute("bodyFooter", footer.toString());

        return "/content/ascii-doc";
    }

    @RequestMapping("/0/sections/{documentName}")
    public String viewSection(ModelMap model, @PathVariable(value = "documentName") String documentName) throws IOException {
        String docPath = new StringBuilder(DOCS_SECTIONS_DIR).append(documentName).toString();
        return viewDoc(model, docPath);
    }

    @RequestMapping("/0/images/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable(value = "imageName") String imageName) throws IOException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        String imgPath = new StringBuilder(DOCS_DIR).append(DOCS_IMAGES_DIR)
                .append(imageName).append(PNG_EXTENSION).toString();

        InputStream inputStream = classLoader.getResourceAsStream(imgPath);
        if (inputStream == null) {
            throw new ResourceNotFoundException();
        }

        return IOUtils.toByteArray(inputStream);
    }
}
