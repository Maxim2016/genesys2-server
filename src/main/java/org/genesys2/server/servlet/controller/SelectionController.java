/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.model.json.UserAccessionList;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.service.DownloadService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Scope("request")
@RequestMapping("/sel")
public class SelectionController extends BaseController {

	@Autowired
	private SelectionBean selectionBean;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private DownloadService downloadService;

	@Autowired
	private AccessionListService accessionListService;

	@Autowired
	private PermissionEvaluator permissionEvaluator;

	@RequestMapping(value = "/")
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		model.addAttribute("pagedData", genesysService.listAccessions(selectionBean.copy(), new PageRequest(page - 1, 50, new Sort("accessionName"))));
		model.addAttribute("selection", selectionBean);

		// Add userAccessionLists when authenticated
		try {
			if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken))
				model.addAttribute("userAccessionLists", accessionListService.getMyLists());
		} catch (Throwable e) {
			_logger.error(e.getMessage(), e);
		}
		return "/selection/index";
	}

	@RequestMapping(value = "/load", params = { "list" })
	@PreAuthorize("isAuthenticated()")
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "list", required = true) UUID uuid) {

		AccessionList list = accessionListService.getList(uuid);
		if (list != null) {
			Set<Long> accessionIds = accessionListService.getAccessionIds(list);
			selectionBean.clear();
			for (Long accessionId : accessionIds) {
				selectionBean.add(accessionId);
			}

			// If I have permissions to manage list:
			if (permissionEvaluator.hasPermission(SecurityContextHolder.getContext().getAuthentication(), list, "WRITE")) {
				if (_logger.isDebugEnabled()) {
					_logger.debug("User has WRITE parmission on AccessionList");
				}
				selectionBean.setUserAccessionList(UserAccessionList.from(list));
			}
		}

		return "redirect:/sel/";
	}

	@RequestMapping(value = "/map")
	public String map(ModelMap model) {
		model.addAttribute("selection", selectionBean);
		return "/selection/map";
	}

	@RequestMapping(value = "/order")
	public String order(ModelMap model) {
		return "redirect:/request";
	}

	@RequestMapping("/add/{id}")
	public String add(ModelMap model, @PathVariable("id") long accessionId) {
		selectionBean.add(accessionId);
		return "redirect:/sel/#a" + accessionId;
	}

	@RequestMapping(method = RequestMethod.POST, value = "add-many")
	public String add(ModelMap model, RedirectAttributes redirectAttrs, @RequestParam(required = true, value = "instCode") String instCode,
			@RequestParam(required = true, value = "accessionIds") String accessionIds) {

		if (StringUtils.isBlank(instCode)) {
			return "redirect:/sel/";
		}

		final String[] splits = accessionIds.split("[,;\\t\\n]+");
		for (final String s : splits) {
			// System.err.println(">> '" + s.trim() + "'");
			try {
				final Accession accession = genesysService.getAccession(instCode, s.trim());
				if (accession != null) {
					// System.err.println("\t" + accession.getId());
					selectionBean.add(accession.getId());
				} else {
					// System.err.println("\t NF='" + s + "'");
				}
			} catch (NonUniqueAccessionException e) {
				// Non-unique
			}
		}
		return "redirect:/sel/";
	}

	@RequestMapping("/remove/{id}")
	public String remove(ModelMap model, @PathVariable("id") long accessionId) {
		selectionBean.remove(accessionId);
		return "redirect:/sel/";
	}

	@RequestMapping("/clear")
	public String remove(ModelMap model) {
		selectionBean.clear();
		return "redirect:/sel/";
	}

	/**
	 * Download DwCA of selected accessions
	 * 
	 * @param model
	 * @param cropName
	 * @param jsonFilter
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/dwca", method = RequestMethod.POST)
	public void dwca(ModelMap model, HttpServletResponse response) throws IOException {
		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		AppliedFilter arr = new FilterHandler.AppliedFilter().setFilterName(FilterConstants.ID);
		for (final long id : selectionBean.copy()) {
			arr.addFilterValue(new FilterHandler.LiteralValueFilter(id));
		}
		appliedFilters.add(arr);

		final int countFiltered = selectionBean.size();
		_logger.info("Attempting to download DwCA for " + countFiltered + " accessions");
		if (countFiltered > 100000) {
			throw new RuntimeException("Refusing to export more than 100,000 entries");
		}

		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-selected.zip\""));

		// Write Darwin Core Archive to the stream.
		final OutputStream outputStream = response.getOutputStream();

		genesysService.writeAccessions(appliedFilters, outputStream);
		response.flushBuffer();
	}

	@RequestMapping(value = "/download/mcpd", method = RequestMethod.POST)
	public void downloadXlsxMCPD(ModelMap model, HttpServletResponse response) throws IOException {
		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		AppliedFilter arr = new FilterHandler.AppliedFilter().setFilterName(FilterConstants.ID);
		for (final long id : selectionBean.copy()) {
			arr.addFilterValue(new FilterHandler.LiteralValueFilter(id));
		}
		appliedFilters.add(arr);

		final int countFiltered = genesysService.countAccessions(appliedFilters);
		_logger.info("Attempting to download XLSX MCPD for " + countFiltered + " accessions");

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-selected.xlsx\""));

		// Write Darwin Core Archive to the stream.
		final OutputStream outputStream = response.getOutputStream();

		downloadService.writeXlsxMCPD(appliedFilters, outputStream);
		response.flushBuffer();
	}

	// TODO REMOVE
	@RequestMapping(value = "/json/count", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Long selectionCount() {
		return (long) selectionBean.size();
	}

	@RequestMapping(value = "/json/selection", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Set<Long> selection() {
		return selectionBean.copy();
	}

	@RequestMapping(value = "/json/geo", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public List<AccessionGeoJson> selectionGeo() {
		final List<AccessionGeoJson> geo = new ArrayList<AccessionGeoJson>();
		if (selectionBean.size() > 0) {
			for (final AccessionGeo acnGeo : genesysService.listAccessionsGeo(selectionBean.copy())) {
				final AccessionGeoJson g = new AccessionGeoJson();
				g.id = acnGeo.getAccession().getId();
				g.lat = acnGeo.getLatitude();
				g.lng = acnGeo.getLongitude();
				AccessionData a = genesysService.getAccession(g.id);
				g.accessionName = a.getAccessionName();
				g.instCode = a.getInstituteCode();
				geo.add(g);
			}
		}
		return geo;
	}

	@RequestMapping(value = "/json", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public JsonResponse jsonOp(@RequestBody JsonAction action) {
		_logger.info("Selection action " + action.action + ": " + action.id);
		final JsonResponse resp = new JsonResponse();
		if ("add".equals(action.action)) {
			selectionBean.add(action.id);
			resp.included = true;
		} else {
			selectionBean.remove(action.id);
			resp.included = false;
		}
		resp.count = selectionBean.size();
		return resp;
	}

	public static class JsonAction {
		public long id;
		public String action;
	}

	public static class JsonResponse {
		public int count;
		public boolean included;
	}

	public static class AccessionGeoJson {
		public long id;
		public Double lat;
		public Double lng;
		public String accessionName;
		public String instCode;
	}

	@RequestMapping(value = "/userList", params = { "save" }, method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public String saveAccessionList(@RequestParam String title, @RequestParam String description, @RequestParam(defaultValue = "false") boolean shared,
			RedirectAttributes redirectAttrs) {

		AccessionList accessionList = new AccessionList();

		return saveOrUpdateList(title, description, shared, redirectAttrs, accessionList);
	}

	@RequestMapping(value = "/userList", params = { "update", "uuid" }, method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public String updateAccessionList(@RequestParam(value = "uuid") UUID uuid, @RequestParam String title, @RequestParam String description,
			@RequestParam(defaultValue = "false") boolean shared, RedirectAttributes redirectAttrs) {

		AccessionList accessionList = accessionListService.getList(uuid);

		return saveOrUpdateList(title, description, shared, redirectAttrs, accessionList);
	}

	/**
	 * Update selected list and load relevant data
	 * 
	 * @param page
	 * @param title
	 * @param description
	 * @param model
	 * @param accessionList
	 * @return
	 */
	private String saveOrUpdateList(String title, String description, boolean shared, RedirectAttributes redirectAttrs, AccessionList accessionList) {
		accessionList.setDescription(description);
		accessionList.setTitle(title);
		accessionList.setShared(shared);

		accessionListService.save(accessionList);
		accessionListService.setList(accessionList, selectionBean.copy());
		selectionBean.setUserAccessionList(UserAccessionList.from(accessionList));

		redirectAttrs.addFlashAttribute("resultFromSave", "user.accession.list.saved-updated");

		return "redirect:/sel/";
	}

	@RequestMapping(value = "/userList", params = { "delete" }, method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public String deleteAccessionList(@RequestParam(value = "uuid") UUID uuid, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
			Model model, RedirectAttributes redirectAttrs) {

		accessionListService.delete(accessionListService.getList(uuid));
		selectionBean.setUserAccessionList(null);

		redirectAttrs.addFlashAttribute("resultFromSave", "user.accession.list.deleted");
		return "redirect:/sel/";
	}

	@RequestMapping(value = "/userList", params = { "disconnect" }, method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public String disconnectAccessionList(Model model) {

		selectionBean.setUserAccessionList(null);

		return "redirect:/sel/";
	}
}
