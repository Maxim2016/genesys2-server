/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.oauth;

import java.util.List;

import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.genesys2.server.servlet.controller.ProfileController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Manage OAuth2 clients
 *
 * @author mobreza
 */
@Controller
@RequestMapping("/admin/oauth")
public class ClientController extends ProfileController<ClientDetails, String> {

	@Autowired
	private OAuth2ClientDetailsService clientDetailsService;

	@Autowired
	private TokenStore tokenStore;

	@Override
	protected ClientDetails getDetails(String identifier) {
		return clientDetailsService.loadClientByClientId(identifier);
	}

	@Override
	public void addDetails(ModelMap model, ClientDetails item) {
		super.addDetails(model, item);
		model.addAttribute("tokens", tokenStore.findTokensByClientId(item.getClientId()));
	}

	@Override
	protected List<OAuthClientDetails> list() {
		return clientDetailsService.listClientDetails();
	}

	@Override
	protected String getViewPrefix() {
		return "/admin/oauth/client";
	}

}
