/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.exception.NotUniqueUserException;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.controller.rest.model.UserChangedDataJson;
import org.genesys2.server.servlet.model.UserList;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller("restUsersController")
@PreAuthorize("isAuthenticated() && hasRole('ADMINISTRATOR')")
@RequestMapping(value = {"/api/v0/users", "/json/v0/users"})
public class UsersController extends RestController {

    @Value("${base.url}")
    private String baseUrl;

    @Autowired
    protected UserService userService;

    @Autowired
    private EMailVerificationService emailVerificationService;

    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
     public Object getUsers(@RequestParam(value = "startRow", required = false, defaultValue = "0") Integer startRow,
                           @RequestParam(value = "pageSize", required = false, defaultValue = "0") Integer pageSize) throws UserException {
        return new UserList(userService.listWrapped(startRow, pageSize));
    }

    @RequestMapping(value = "/available_roles", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getAvailableRoles() throws UserException {
        return userService.listAvailableRoles();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getUser(@PathVariable Long id) throws UserException {
        Map <String, Object> resultMap = new HashMap<>();
        User user = userService.getUserById(id);
        resultMap.put("user", user);
        resultMap.put("userTeams", teamService.listUserTeams(user));
        return resultMap;
    }

    @RequestMapping(value = "/user/uuid/{id:.+}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getUserByUuid(@PathVariable String id) throws UserException {
        return userService.getUserByUuid(id);
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public void saveUser(@RequestBody @Validated User user) throws UserException {
        userService.addUser(user);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public void updateUser(@RequestBody @Validated User user) throws UserException {
        try {
            userService.updateUser(user);
        } catch (final DataIntegrityViolationException e) {
            throw new NotUniqueUserException(e, user.getEmail());
        }
    }

    @RequestMapping(value = "/user/data", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String updateData(@RequestBody UserChangedDataJson userData) {
        final User user = userService.getUserByUuid(userData.getUuid());

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        try {
            userService.updateData(user.getId(), userData.getName(), userData.getEmail());
        } catch (NotUniqueUserException e) {
            LOG.warn("User with e-mail address " + e.getEmail() + " already exists");
        } catch (UserException e) {
            LOG.warn("E-mail address is incorrect");
        }

        if (StringUtils.isNotBlank(userData.getPwd1())) {
            if (userData.getPwd1().equals(userData.getPwd2())) {
                try {
                    LOG.info("Updating password for " + user);
                    userService.updatePassword(user.getId(), userData.getPwd1());
                    LOG.warn("Password updated for " + user);
                } catch (final UserException e) {
                    LOG.error(e.getMessage(), e);
                }
            } else {
                LOG.warn("Passwords didn't match for " + user);
            }
        }

        return JSON_OK;
    }

    @RequestMapping(value = "/user/roles", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String updateRoles(@RequestBody UserChangedDataJson userData) {
        final User user = userService.getUserByUuid(userData.getUuid());
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        userService.updateRoles(user, Arrays.asList(userData.getRoles()));
        return JSON_OK;
    }

    @RequestMapping(value = "user/{uuid:.+}/send", method = RequestMethod.GET)
    @ResponseBody
    public String sendEmail(@PathVariable("uuid") String uuid) {

        final User user = userService.getUserByUuid(uuid);
        emailVerificationService.sendVerificationEmail(user);

        return JSON_OK;
    }

    @RequestMapping("user/{uuid:.+}/vetted-user")
    @ResponseBody
    public String addRoleVettedUser(@PathVariable("uuid") String uuid) {
        userService.addVettedUserRole(uuid);
        return JSON_OK;
    }


}
