/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.Map;

import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.TraitCode;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller which simply handles *.html requests
 */
@Controller
@RequestMapping("/descriptors")
public class DescriptorController extends BaseController {

	@Autowired
	private TraitService traitService;

	@RequestMapping("/categories")
	public String viewCategories(ModelMap model) {
		model.addAttribute("categories", traitService.listCategories());
		return "/descr/categories";
	}

	@RequestMapping
	public String index(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("pagedData", traitService.listTraits(new PageRequest(page - 1, 50, new Sort("title"))));

		return "/descr/index";
	}

	@RequestMapping("/{id}")
	public String view(ModelMap model, @PathVariable("id") long traitId) {
		final Parameter trait = traitService.getTrait(traitId);
		if (trait == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("trait", trait);
		model.addAttribute("traitMethods", traitService.getTraitMethods(trait));

		return "/descr/details";
	}

	/**
	 * Redirect "/descriptor/gm:1234" URLs to "/descriptor/{traitId}/{methodId}"
	 */
	@RequestMapping("/gm:{methodId}")
	public String viewMethod(ModelMap model, @PathVariable("methodId") long methodId) {
		final Method method = traitService.getMethod(methodId);
		if (method == null) {
			throw new ResourceNotFoundException();
		}
		return "redirect:/descriptors/" + method.getParameter().getId() + "/" + method.getId();
	}

	@RequestMapping("/{traitId}/{methodId}")
	public String view(ModelMap model, @PathVariable("traitId") long traitId, @PathVariable("methodId") long methodId) {
		final Parameter trait = traitService.getTrait(traitId);
		if (trait == null) {
			throw new ResourceNotFoundException();
		}
		final Method method = traitService.getMethod(methodId);
		if (method == null) {
			throw new ResourceNotFoundException();
		}
		if (!method.getParameter().getId().equals(trait.getId())) {
			_logger.warn("Method does not belong to Param");
		}

		model.addAttribute("trait", trait);
		model.addAttribute("method", method);
		model.addAttribute("metadatas", traitService.listMetadataByMethod(method));

		if (method.isCoded()) {
			final Map<String, String> codeMap = TraitCode.parseCodeMap(method.getOptions());
			model.addAttribute("codeMap", codeMap);
			model.addAttribute("codeStatistics", traitService.getMethodStatistics(method));
		}

		return "/descr/method";
	}
}
