/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.PDCI;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.StatisticsService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.server.service.impl.PDCICalculator;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/acn")
public class AccessionController extends BaseController {

	public static final UUID WORLDCLIM_DATASET_UUID = UUID.fromString("BC84433B-A626-4BDF-97D3-DB36D79499C6");

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TraitService traitService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private CropService cropService;

	@Autowired
	private DSService dsService;

	@Autowired(required = false)
	private PDCICalculator pdciCalculator;

	@Autowired
	private StatisticsService statisticsService;

	@RequestMapping("/id/{accessionId}")
	public String view(ModelMap model, @PathVariable(value = "accessionId") long accessionId) {
		_logger.debug("Viewing ACN " + accessionId);
		final Accession accession = genesysService.getAccession(accessionId);
		if (accession == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("accession", accession);
		model.addAttribute("accessionNames", genesysService.listAccessionNames(accession.getAccessionId()));
		model.addAttribute("accessionAliases", genesysService.listAccessionAliases(accession.getAccessionId()));
		model.addAttribute("accessionExchange", genesysService.listAccessionExchange(accession.getAccessionId()));
		model.addAttribute("accessionCollect", genesysService.listAccessionCollect(accession.getAccessionId()));
		model.addAttribute("accessionBreeding", genesysService.listAccessionBreeding(accession.getAccessionId()));
		AccessionGeo accessionGeo = genesysService.listAccessionGeo(accession.getAccessionId());
		model.addAttribute("accessionGeo", accessionGeo);
		model.addAttribute("svalbardData", genesysService.getSvalbardData(accession.getAccessionId()));
		model.addAttribute("accessionRemarks", genesysService.listAccessionRemarks(accession.getAccessionId()));

		model.addAttribute("metadatas", genesysService.listMetadata(accession.getAccessionId()));
		model.addAttribute("methods", traitService.listMethods(accession.getAccessionId()));
		model.addAttribute("methodValues", genesysService.getAccessionTraitValues(accession.getAccessionId()));

		if (accession.getCrop() != null) {
			List<Crop> crops = new ArrayList<Crop>();
			crops.add(accession.getCrop());
			model.addAttribute("crops", crops);
		} else {
			// TODO Remove
			// model.addAttribute("crops", cropService.getCrops(accession.getTaxonomy()));
		}

		// Worldclim data
		if (accessionGeo != null && accessionGeo.getTileIndex() != null) {
			DS worldClimDataset = dsService.loadDatasetByUuid(WORLDCLIM_DATASET_UUID);
			if (worldClimDataset != null) {
				model.addAttribute("worldclimJson", dsService.jsonForTile(worldClimDataset, accessionGeo.getTileIndex()));
			}
		}

		// PDCI
		if (pdciCalculator != null) {
			try {
				// FIXME Duplicated loading of all accession information
				PDCI pdci = pdciCalculator.makePDCI(accession.getId());

				PDCI existingPdci = genesysService.loadPDCI(accession.getId());

				if (existingPdci == null) {
					existingPdci = genesysService.updatePDCI(accession.getId());
				}

				if (existingPdci != null && pdci.getTotal() != existingPdci.getTotal()) {
					_logger.info("Forcing recalculation of PDCI");
					genesysService.updatePDCI(accession.getId());
				}

				model.addAttribute("pdci", pdci);
				model.addAttribute("institutePDCI", statisticsService.statisticsPDCI(accession.getInstitute()));

			} catch (Throwable e) {
				_logger.warn(e.getMessage(), e);
			}
		}

		return "/accession/details";
	}

	@RequestMapping("/{holdingInstitute}/{genus}/{accessionName:.+}")
	public String viewInstituteAccession(ModelMap model, @PathVariable(value = "holdingInstitute") String holdingInstitute,
			@PathVariable(value = "genus") String genus, @PathVariable(value = "accessionName") String accessionName) {
		_logger.debug("Viewing ACN " + accessionName);
		final FaoInstitute faoInstitute = instituteService.getInstitute(holdingInstitute);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		try {
			final Accession accession = genesysService.getAccession(holdingInstitute, accessionName, genus);
			if (accession == null) {
				throw new ResourceNotFoundException();
			} else {
				return "redirect:/acn/id/" + accession.getId();
			}
		} catch (NonUniqueAccessionException e) {
			return "redirect:/acn/" + holdingInstitute + "/" + accessionName;
		}
	}

	@RequestMapping("/{holdingInstitute}/{accessionName:.+}")
	public String viewInstituteAccession(ModelMap model, @PathVariable(value = "holdingInstitute") String holdingInstitute,
			@PathVariable(value = "accessionName") String accessionName) {
		_logger.debug("Viewing ACN " + accessionName);
		final FaoInstitute faoInstitute = instituteService.findInstitute(holdingInstitute);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		final List<Accession> accessions = genesysService.listAccessions(faoInstitute, accessionName);
		if (accessions.size() == 0) {
			throw new ResourceNotFoundException();
		}
		if (accessions.size() == 1) {
			return "redirect:/acn/id/" + accessions.get(0).getId();
		}

		model.addAttribute("faoInstitute", faoInstitute);
		// This expects a List<>
		model.addAttribute("accessions", accessions);

		return "/accession/resolve";
	}

	/**
	 * View by Taxonomy
	 * 
	 * @param model
	 * @param wiewsCode
	 * @param genus
	 * @param page
	 * @return
	 */
	@RequestMapping("/t/{genus}")
	public String viewDataByGenus(ModelMap model, @PathVariable(value = "genus") String genus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		// Taxonomy
		final Taxonomy2 taxonomy = taxonomyService.get(genus);
		if (taxonomy == null) {
			throw new ResourceNotFoundException("No taxonomy with genus=" + genus);
		}

		model.addAttribute("filter", "{\"" + FilterConstants.TAXONOMY_GENUS + "\":[\"" + taxonomy.getGenus() + "\"]}");
		model.addAttribute("page", page);

		return "redirect:/explore";
	}

	/**
	 * View by Taxonomy
	 * 
	 * @param model
	 * @param wiewsCode
	 * @param genus
	 * @param species
	 * @param page
	 * @return
	 */
	@RequestMapping("/t/{genus}/{species:.+}")
	public String viewDataByGenusSpecies(ModelMap model, @PathVariable(value = "genus") String genus, @PathVariable(value = "species") String species,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		// Taxonomy
		final Taxonomy2 taxonomy = taxonomyService.get(genus, species);
		if (taxonomy == null) {
			throw new ResourceNotFoundException("No taxonomy with genus=" + genus);
		}

		model.addAttribute("filter", "{\"" + FilterConstants.TAXONOMY_GENUS + "\":[\"" + taxonomy.getGenus() + "\"], \"" + FilterConstants.TAXONOMY_SPECIES
				+ "\":[\"" + taxonomy.getSpecies() + "\"]}");
		model.addAttribute("page", page);

		return "redirect:/explore";
	}

}
