/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.cursors.ObjectObjectCursor;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/admin/elastic")
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class ElasticSearchController {
	public static final Log LOG = LogFactory.getLog(ElasticSearchController.class);

	@Autowired
	ElasticUpdater elasticUpdater;

	@Autowired
	ElasticService elasticService;

	@Autowired
	private Client client;

	ObjectMapper mapper = new ObjectMapper();

	/**
	 * Renders view where indexes and their aliases are displayed.
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/")
	public String viewIndexesAndAliases(Model model) {
		ImmutableOpenMap<String, IndexMetaData> indicesImmutableMap = client.admin().cluster().prepareState().execute().actionGet().getState().getMetaData()
				.getIndices();

		Map<String, List<AliasMetaData>> indexMap = new HashMap<>();
		for (ObjectObjectCursor<String, IndexMetaData> cursor : indicesImmutableMap) {
			List<AliasMetaData> innerAliasList = new ArrayList<>();
			for (ObjectObjectCursor<String, AliasMetaData> innerCursor : cursor.value.getAliases()) {
				innerAliasList.add(innerCursor.value);
			}
			indexMap.put(cursor.key, innerAliasList);
		}

		model.addAttribute("indexes", indexMap);

		return "/admin/elastic/index";
	}

	/**
	 * Completely recreate Elasticsearch indexes: create, index, re-alias.
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "regenerate" })
	public String regenerateElastic() {
		elasticService.regenerateIndexes();
		return "redirect:/admin/elastic/";
	}

	/**
	 * This method refreshes data in the currently active index. It is very
	 * handy when having to refresh part of ES after direct database update.
	 *
	 * @param jsonFilter
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "reindex", "filter" })
	public String reindexElasticFiltered(@RequestParam(value = "filter", required = true) String jsonFilter) throws IOException {

		FilterHandler.AppliedFilters filters = mapper.readValue(jsonFilter, FilterHandler.AppliedFilters.class);
		elasticService.reindex(filters);
		return "redirect:/admin/elastic/";
	}

	/**
	 * Clear ES queue
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "clear-queues" })
	public String clearElasticQueues() {
		elasticUpdater.clearQueues();
		return "redirect:/admin/elastic/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "action=realias" })
	public String moveAlias(@RequestParam(name = "aliasName") String aliasName, @RequestParam(name = "indexName") String indexName) {
		elasticService.realias(aliasName, indexName);
		return "redirect:/admin/elastic/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action", params = { "action=delete-alias" })
	public String deleteAlias(@RequestParam(name = "aliasName") String aliasName) {
		elasticService.deleteAlias(aliasName);
		return "redirect:/admin/elastic/";
	}
}
