/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.io.IOException;
import java.util.Set;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.test.JpaRealDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.monitor.LocalMapStats;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AccessionCacheTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@Ignore
@DirtiesContext
public class AccessionCacheTest {

	@Import({ JpaRealDataConfig.class, HazelcastConfig.class })
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}
	}

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private InstituteService instituteService;

	@Test
	public void test1() {
		FaoInstitute iita = instituteService.getInstitute("NGA039");
		System.out.println("IITA: " + iita);

		System.err.println("See caching of country");
		for (int i = 0; i < 10; i++) {
			iita = instituteService.getInstitute("NGA039");
			System.out.println("Country: " + iita.getCountry());
		}
		printCacheStats();
	}

	@Test
	public void test2() {
		System.out.println("Find 1st");
		Accession accession = genesysService.getAccession(3446120);
		System.out.println("Acn: " + accession);
		System.out.println(accession.getCountryOfOrigin());
		System.out.println(accession.getInstitute());
		System.out.println(accession.getTaxonomy());

		System.out.println("See caching of stuff");
		for (int i = 0; i < 10; i++) {
			System.out.println("Find");
			accession = genesysService.getAccession(3446120);
			System.out.println("Acn: " + accession);
			System.out.println(accession.getCountryOfOrigin());
			System.out.println(accession.getInstitute());
			System.out.println(accession.getTaxonomy());
		}
		printCacheStats();

	}

	public static void printCacheStats() {
		Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
		for (HazelcastInstance hz : instances) {
			System.out.println("\n\nCache stats Instance: " + hz.getName());
			for (DistributedObject o : hz.getDistributedObjects()) {
				if (o instanceof IMap) {
					IMap imap = (IMap) o;
					System.out.println(imap.getServiceName() + ": " + imap.getName() + " " + imap.getPartitionKey());
					LocalMapStats localMapStats = imap.getLocalMapStats();
					System.out.println("created: " + localMapStats.getCreationTime());
					System.out.println("owned entries: " + localMapStats.getOwnedEntryCount());
					System.out.println("backup entries: " + localMapStats.getBackupEntryCount());
					System.out.println("locked entries: " + localMapStats.getLockedEntryCount());
					System.out.println("dirty entries: " + localMapStats.getDirtyEntryCount());
					System.out.println("hits: " + localMapStats.getHits());
					System.out.println("puts: " + localMapStats.getPutOperationCount());
					System.out.println("last update: " + localMapStats.getLastUpdateTime());
					System.out.println("last access:" + localMapStats.getLastAccessTime());
				} else {
					System.out.println(o.getClass() + " " + o);
				}
			}
		}
	}
}
