/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.TaxonomyManager;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.spring.config.SpringCacheConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TaxonomyEnsureTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@Ignore
public class TaxonomyEnsureTest {

	@Import({ JpaNoCacheDataConfig.class, SpringCacheConfig.class })
	public static class Config {
		
		@Bean
		public TaxonomyManager taxonomyManager() {
			return new TaxonomyManager();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

	}

	// use it without @Scheduled
	@Autowired
	private TaxonomyService taxonomyService;
	
	@Autowired
	private TaxonomyManager taxonomyManager;

	@Test
	public void testSubstr() {
		final String genus = "Aaaa";
		final String species = "bbbb";
		final String taxonName = "Aaaa bbbb fb.";
		final String x = genus + ' ' + species;

		assertTrue(taxonName.startsWith(x));

		if (taxonName.startsWith(x)) {
			final String subtaxa = taxonName.substring(x.length()).trim();
			assertTrue(StringUtils.isNotBlank(subtaxa));
			assertTrue("fb.".equals(subtaxa));
		}
	}

	@Test
	public void testEnsure1() {
		try {
			final Taxonomy2 tax = taxonomyManager.ensureTaxonomy2("Aaaaa", "bbbb", null, "alfalfa", null);
			assertTrue("id missing", tax.getId() != null);
			assertTrue("taxGenus should not be null", tax.getTaxGenus() != null);
			assertTrue("taxSpecies should not be null", tax.getTaxSpecies() != null);
			assertFalse("taxGenus should not be this", tax.getTaxGenus().equals(tax.getId()));
			assertFalse("taxSpecies should not be this", tax.getTaxSpecies().equals(tax.getId()));
			assertFalse("taxSpecies should not match taxGenus", tax.getTaxSpecies().equals(tax.getTaxGenus()));

			final Taxonomy2 taxGenus = taxonomyService.get(tax.getTaxGenus());
			assertTrue("taxGenus should have species=sp. and not " + taxGenus.getSpecies(), taxGenus.getSpecies().equals("sp."));

			final Taxonomy2 taxSpecies = taxonomyService.get(tax.getTaxSpecies());
			assertTrue("taxSpecies should have species=bbbb and not " + taxSpecies.getSpecies(), taxSpecies.getSpecies().equals(tax.getSpecies()));

		} catch (final Exception e) {
			fail("This should not have any exceptions!");
		}
	}

	@Test
	public void testNullSpecies() {
		try {
			final Taxonomy2 tax = taxonomyManager.ensureTaxonomy2("Aaaaa", null, null, null, null);
			assertTrue("id missing", tax.getId() != null);
			assertTrue("taxGenus should not be null", tax.getTaxGenus() != null);
			assertTrue("taxSpecies should not be null", tax.getTaxSpecies() != null);
			assertTrue("taxGenus must be this", tax.getTaxGenus().equals(tax.getId()));
			assertTrue("taxSpecies must be this", tax.getTaxSpecies().equals(tax.getId()));
			assertTrue("taxSpecies should match taxGenus", tax.getTaxSpecies().equals(tax.getTaxGenus()));
		} catch (final Exception e) {
			fail("This should not have any exceptions!");
		}
	}

	@Test
	public void testSpSpecies() {
		try {
			final Taxonomy2 tax = taxonomyManager.ensureTaxonomy2("Aaaaa", "sp.", null, null, null);
			assertTrue("id missing", tax.getId() != null);
			assertTrue("species must be sp.", tax.getSpecies().equals("sp."));
			assertTrue("taxGenus should not be null", tax.getTaxGenus() != null);
			assertTrue("taxSpecies should not be null", tax.getTaxSpecies() != null);
			assertTrue("taxGenus must be this", tax.getTaxGenus().equals(tax.getId()));
			assertTrue("taxSpecies must be this", tax.getTaxSpecies().equals(tax.getId()));
			assertTrue("taxSpecies should match taxGenus", tax.getTaxSpecies().equals(tax.getTaxGenus()));
		} catch (final Exception e) {
			fail("This should not have any exceptions!");
		}
	}

	@Test
	public void testSpecies() {
		try {
			final Taxonomy2 tax = taxonomyManager.ensureTaxonomy2("Aaaaa", "species", null, null, null);
			assertTrue("id missing", tax.getId() != null);
			assertTrue("species must be species", tax.getSpecies().equals("species"));
			assertTrue("taxGenus should not be null", tax.getTaxGenus() != null);
			assertTrue("taxSpecies should not be null", tax.getTaxSpecies() != null);
			assertFalse("taxGenus must not be this", tax.getTaxGenus().equals(tax.getId()));
			assertTrue("taxSpecies must be this", tax.getTaxSpecies().equals(tax.getId()));
			assertFalse("taxSpecies should not match taxGenus", tax.getTaxSpecies().equals(tax.getTaxGenus()));
		} catch (final Exception e) {
			fail("This should not have any exceptions!");
		}
	}

	@Test
	public void testFullTaxonomy() {
		try {
			final Taxonomy2 tax = taxonomyManager.ensureTaxonomy2("Aaaaa", "species", "spauthor", "subtaxa", "subtauthor");
			System.out.println(tax);
			assertTrue("id missing", tax.getId() != null);
			assertTrue("species must be sp.", tax.getSpecies().equals("species"));
			assertTrue("taxGenus should not be null", tax.getTaxGenus() != null);
			assertTrue("taxSpecies should not be null", tax.getTaxSpecies() != null);
			assertFalse("taxGenus must not be this", tax.getTaxGenus().equals(tax.getId()));
			assertFalse("taxSpecies must not be this", tax.getTaxSpecies().equals(tax.getId()));
			assertFalse("taxSpecies should not match taxGenus", tax.getTaxSpecies().equals(tax.getTaxGenus()));

			final Taxonomy2 taxGenus = taxonomyService.get(tax.getTaxGenus());
			System.out.println(taxGenus);

			final Taxonomy2 taxSpecies = taxonomyService.get(tax.getTaxSpecies());
			System.out.println(taxSpecies);

		} catch (final Exception e) {
			fail("This should not have any exceptions!");
		}
	}
}
