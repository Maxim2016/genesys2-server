/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import java.io.IOException;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.config.PlaceholderConfigurerSupport;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * Initializing class for using "context:property-placeholder" within @Configuration
 * classes
 */
public class PropertyPlacholderInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
		final PlaceholderConfigurerSupport postProcessor = new PropertySourcesPlaceholderConfigurer();
		try {
			postProcessor.setLocations(
			// add all properties
					resourcePatternResolver.getResources("classpath:**/*.properties"));
		} catch (final IOException e) {
			throw new BeanCreationException("Could not create bean " + postProcessor.toString(), e);
		}
		applicationContext.addBeanFactoryPostProcessor(postProcessor);
	}
}
