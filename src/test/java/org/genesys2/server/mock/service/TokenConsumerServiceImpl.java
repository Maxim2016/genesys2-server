/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.mock.service;

import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Mock service to test verification token behavior with exceptions
 *
 * @author matijaobreza
 *
 */
@Service
@Transactional(readOnly = true)
public class TokenConsumerServiceImpl implements TokenConsumerService {

	@Autowired
	private TokenVerificationService tokenVerificationService;

	@Override
	@Transactional
	public void throwRuntimeException(VerificationToken token) throws NoSuchVerificationTokenException {
		tokenVerificationService.consumeToken(token.getPurpose(), token.getUuid(), token.getKey());
		throw new RuntimeException();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void throwException(VerificationToken token) throws Exception {
		tokenVerificationService.consumeToken(token.getPurpose(), token.getUuid(), token.getKey());
		throw new Exception();
	}

	@Override
	@Transactional
	public void noExceptions(VerificationToken token) throws NoSuchVerificationTokenException {
		tokenVerificationService.consumeToken(token.getPurpose(), token.getUuid(), token.getKey());
	}

	@Override
	@Transactional
	public void noToken() throws NoSuchVerificationTokenException {
		tokenVerificationService.consumeToken("nopurpose", "no-such-uuid", "wrongkey");
		throw new RuntimeException("Should not get here");
	}
}
