/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class OWASPSanitaizerTest extends AbstractServicesTest {

	@Test
	public void sanitizeTest() {

		String inputHtml = "<html>\n" + "<head>\n" + "    <title>Sanitize test</title>\n" + "</head>\n" + "<body>\n" + "\n" + "<div class=\"container\">\n"
				+ "    <a>link</a>\n" + "    <p>paragraph</p>\n" + "</div>\n" + "\n" + "<script>\n" + "    function doSomethink(e){\n"
				+ "        alert('Sanitizer');\n" + "    }\n" + "</script>\n" + "</body>\n" + "</html>";

		String expectedHtml = "<div class=\"container\">\n" + "    link\n" + "    <p>paragraph</p>\n" + "</div>";

		assertThat("HTML not sanitized", htmlSanitizer.sanitize(inputHtml).trim(), equalTo(expectedHtml));
	}
}
