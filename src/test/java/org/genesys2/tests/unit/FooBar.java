package org.genesys2.tests.unit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FooBar {
	@Bean
	public CacheManager cacheManager() {
		return new NoOpCacheManager();
	}
}
