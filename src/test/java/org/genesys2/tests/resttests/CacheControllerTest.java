package org.genesys2.tests.resttests;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@EnableCaching
public class CacheControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(CacheControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    }

    @Test
    public void cacheStatsTest() throws Exception {
        LOG.info("Start test-method cacheStatsTest");

        mockMvc.perform(get("/api/v0/cache")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.cacheMaps", hasSize(greaterThan(0))))
                .andExpect(jsonPath("$.cacheOther", hasSize(0)));

        LOG.info("Test cacheStatsTest passed.");
    }

    @Ignore
    @Test
    public void clearTilesCacheTest() throws Exception {
        LOG.info("Start test-method clearTilesCacheTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/cache/clearTilesCache")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test clearTilesCacheTest passed.");
    }

    @Test
    public void clearCachesTest() throws Exception {
        LOG.info("Start test-method clearCachesTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/cache/clearCaches")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test clearCachesTest passed.");
    }
}
