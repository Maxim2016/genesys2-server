package org.genesys2.tests.resttests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class CropsControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(CropsControllerTest.class);

	@Rule
	public final RestDocumentation restDocumentation = new RestDocumentation("target/generated-snippets");

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mockMvc;
    private static final ObjectMapper objectMapper;
    
    private Crop crop;
    private CropRule cropRule;

    static {
    	objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
    }
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(documentationConfiguration(this.restDocumentation).uris()
                .withScheme("https")
                .withHost("sandbox.genesys-pgr.org")
                .withPort(443)).build();
        
        crop = cropService.addCrop("maize", "Maize", "Crop description in EN", null);

        cropRule = cropService.addCropRule(crop, "Zea", "mays", true);

        List<CropRule> cropRules = new ArrayList<>();
        cropRules.add(cropRule);
        crop.setCropRules(cropRules);
    }

    @After
    public void tearDown() {
        cropRepository.deleteAll();
    }

    @Test
    public void listCropsTest() throws Exception {
        LOG.info("Start test-method listCropsTest");

        mockMvc.perform(get("/api/v0/crops")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].version", is(0)))
                .andExpect(jsonPath("$[0].shortName", is("maize")))
                .andExpect(jsonPath("$[0].i18n", is(nullValue())))
                .andExpect(jsonPath("$[0].name", is("Maize")))
                .andExpect(jsonPath("$[0].description", is("Crop description in EN")))
                .andDo(document("crop-list"));

        LOG.info("Test listCropsTest passed");
    }

    @Test
    public void createCropTest() throws Exception {
        LOG.info("Start test-method createCropTest");

        cropRepository.deleteAll();

        mockMvc.perform(post("/api/v0/crops")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Object() {
                	public String shortName="maize";
                	public String name="Maize";
                	public String description="Crop description in EN";
                })))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("maize")))
                .andExpect(jsonPath("$.i18n", is(nullValue())))
                .andExpect(jsonPath("$.name", is("Maize")))
                .andExpect(jsonPath("$.description", is("Crop description in EN")))
                .andDo(document("crop-create", 
                		requestFields(
                				fieldWithPath("shortName").description("Crop short name or code (e.g. maize)"),
                				fieldWithPath("name").description("Crop name in English"),
                				fieldWithPath("description").optional().description("Crop description in English")
                		),
                		responseFields( 
                				fieldWithPath("id").description("Autogenerated ID"),
                				fieldWithPath("version").description("Record version"),
		                        fieldWithPath("name").description("Crop name in English"), 
		                        fieldWithPath("shortName").description("Crop short name or code (e.g. maize)"),
		                        fieldWithPath("description").description("Crop description in English"),
		                        fieldWithPath("rdfUri").type(JsonFieldType.STRING).optional().description("URI of RDF term describing the crop"),
		                        fieldWithPath("i18n").type(JsonFieldType.STRING).optional().description("i18n map"),
		                        fieldWithPath("createdBy").ignored(),
		                        fieldWithPath("createdDate").ignored(),
		                        fieldWithPath("lastModifiedBy").ignored(),
		                        fieldWithPath("lastModifiedDate").ignored()
                        )));

        LOG.info("Test createCropTest passed");
    }

    @Test
    public void getCropTest() throws Exception {
        LOG.info("Start test-method getCropTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("maize")))
                .andExpect(jsonPath("$.i18n", is(nullValue())))
                .andExpect(jsonPath("$.name", is("Maize")))
                .andExpect(jsonPath("$.description", is("Crop description in EN")))
                .andDo(document("crop-get", pathParameters( 
                        parameterWithName("shortName").description("Crop short name or code (e.g. maize)")), 
                		responseFields( 
        				fieldWithPath("id").description("Autogenerated ID"),
        				fieldWithPath("version").description("Record version"),
                        fieldWithPath("name").description("Crop name in English"), 
                        fieldWithPath("shortName").description("Crop short name or code (e.g. maize)"),
                        fieldWithPath("description").description("Crop description in English"),
                        fieldWithPath("rdfUri").type(JsonFieldType.STRING).optional().description("URI of RDF term describing the crop"),
                        fieldWithPath("i18n").type(JsonFieldType.STRING).optional().description("i18n map"),
                        fieldWithPath("createdBy").ignored(),
                        fieldWithPath("createdDate").ignored(),
                        fieldWithPath("lastModifiedBy").ignored(),
                        fieldWithPath("lastModifiedDate").ignored()
                )));

        LOG.info("Test getCropTest passed");
    }

    @Test
    public void deleteCropTest() throws Exception {
        LOG.info("Start test-method deleteCropTest");

        mockMvc.perform(delete("/api/v0/crops/{shortName}", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("maize")))
                .andExpect(jsonPath("$.i18n", is(nullValue())))
                .andExpect(jsonPath("$.name", is("Maize")))
                .andExpect(jsonPath("$.description", is("Crop description in EN")))
                .andDo(document("crop-delete", pathParameters( 
                        parameterWithName("shortName").description("Crop short name or code (e.g. maize)")
                )));

        LOG.info("Test deleteCropTest passed");
    }

    @Test
    public void getCropDescriptorsTest() throws Exception {
        LOG.info("Start test-method getCropDescriptorsTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/descriptors", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(0)))
                //.andExpect(jsonPath("$.lastPage", is(true)))
                .andExpect(jsonPath("$.numberOfElements", is(0)))
                .andExpect(jsonPath("$.totalElements", is(0)))
                .andExpect(jsonPath("$.number", is(0)))
//                .andExpect(jsonPath("$.firstPage", is(true)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.totalPages", is(0)))
                .andExpect(jsonPath("$.size", is(50)))
                .andDo(document("crop-descriptor-list", pathParameters( 
                        parameterWithName("shortName").description("Crop short name or code (e.g. maize)")
                )));

        LOG.info("Test getCropDescriptorsTest passed");
    }

    @Test
    public void getCropRulesTest() throws Exception {
        LOG.info("Start test-method getCropRulesTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/rules", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].included", is(true)))
                .andExpect(jsonPath("$[0].genus", is("Zea")))
                .andExpect(jsonPath("$[0].species", is("mays")))
                .andExpect(jsonPath("$[0].subtaxa", is(nullValue())))
                .andDo(document("crop-rules-list", pathParameters( 
                        parameterWithName("shortName").description("Crop short name or code (e.g. maize)")
                )));

        LOG.info("Test getCropRulesTest passed");
    }

    // FIXME Authorization
    @Ignore
    @Test
    public void updateCropRulesTest() throws Exception {
        LOG.info("Start test-method updateCropRulesTest");

        ObjectMapper objectMapper = new ObjectMapper();
        List<CropRule> cropRules = new ArrayList<>();
        CropRule cropRule=new CropRule();
        cropRule.setIncluded(true);
        cropRule.setGenus("Zea");
        cropRule.setSpecies("mays");
        cropRule.setSubtaxa("var. foobaria");
        cropRules.add(cropRule);

        mockMvc.perform(put("/api/v0/crops/{shortName}/rules", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cropRules)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].included", is(true)))
                .andExpect(jsonPath("$[0].genus", is("Zea")))
                .andExpect(jsonPath("$[0].species", is("mays")))
                .andExpect(jsonPath("$[0].subtaxa", is("var. foobaria")))
                .andDo(document("crop-rules-update", pathParameters( 
                        parameterWithName("shortName").description("Crop short name or code (e.g. maize)")
                ), requestFields(
                		fieldWithPath("[].id").ignored(),
        				fieldWithPath("[].included").description("Specifies whether the rule is an including or an excluding rule"),
        				fieldWithPath("[].genus").description("Accession is included in the crop when its genus matches specified value."),
        				fieldWithPath("[].species").optional().description("Accession is included in the crop when its species matches specified value."),
        				fieldWithPath("[].subtaxa").optional().description("Accession is included in the crop when its subtaxa matches specified value.")
        		)));

        LOG.info("Test updateCropRulesTest passed");
    }

    @Test
    public void getCropTaxaTest() throws Exception {
        LOG.info("Start test-method getCropTaxaTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/taxa", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(0)))
//                .andExpect(jsonPath("$.lastPage", is(true)))
                .andExpect(jsonPath("$.numberOfElements", is(0)))
                .andExpect(jsonPath("$.totalElements", is(0)))
                .andExpect(jsonPath("$.number", is(0)))
//                .andExpect(jsonPath("$.firstPage", is(true)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.totalPages", is(0)))
                .andExpect(jsonPath("$.size", is(50)));

        LOG.info("Test getCropTaxaTest passed");
    }

    // FIXME Authentication
    @Ignore
    @Test
    public void rebuildTest() throws Exception {
        LOG.info("Start test-method rebuildTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/crops/rebuild")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test rebuildTest passed");
    }

    @Test
    @Transactional
    public void rebuildCropTest() throws Exception {
        LOG.info("Start test-method rebuildCropTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/crops/{shortName}/rebuild", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test rebuildCropTest passed");
    }
}
