package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.model.genesys.MaterialSubRequest;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.ClassPK;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.impl.EasySMTAException;
import org.genesys2.server.service.impl.RequestServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional
@Ignore
public class RequestsControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(RequestsControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private MaterialRequest materialRequest;
    private MaterialSubRequest materialSubRequest;
    private Article article;
    Article secondArticle = new Article();

    private FaoInstitute faoInstitute;
    private List<FaoInstitute> institutes = new ArrayList<>();

    @Before
    public void setUp() throws UserException, JsonProcessingException, EasySMTAException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        article = new Article();
        article.setBody("BodyArticle");
        article.setSlug("smtp.material-confirm-no-pid");
        article.setLang("en");

        secondArticle.setBody("BodyArticle");
        secondArticle.setSlug("smtp.material-request");
        secondArticle.setLang("en");

        ClassPK classPK = contentService.ensureClassPK(Article.class);

        article.setClassPk(classPK);
        secondArticle.setClassPk(classPK);
        List<Article> listArticles = new ArrayList<>();
        listArticles.add(article);
        listArticles.add(secondArticle);
        contentService.save(listArticles);

        ObjectMapper objectMapper = new ObjectMapper();

        Set<Long> accessionsId = new HashSet<>();
        accessionsId.add(1L);

        RequestService.RequestInfo requestInfo = new RequestService.RequestInfo();
        requestInfo.setEmail("salexandrbasov@gmail.com");
        requestInfo.setNotes("notes");
        requestInfo.setPreacceptSMTA(false);
        requestInfo.setPurposeType(1);

        RequestServiceImpl.RequestBody requestBody = new RequestServiceImpl.RequestBody();
        requestBody.pid = easySMTAConnector.getUserData("salexandrbasov@gmail.com");
        requestBody.requestInfo = requestInfo;
        requestBody.accessionIds = accessionsId;

        materialRequest = new MaterialRequest();
        materialRequest.setEmail("salexandrbasov@gmail.com");
        materialRequest.setVersion(1);
        materialRequest.setBody(objectMapper.writeValueAsString(requestBody));
        materialRequestRepository.save(materialRequest);

        materialSubRequest = new MaterialSubRequest();
        materialSubRequest.setBody(objectMapper.writeValueAsString(requestBody));
        materialSubRequest.setInstCode("Code");
        materialSubRequest.setVersion(2);
        materialSubRequest.setSourceRequest(materialRequest);

        materialSubRequestRepository.save(materialSubRequest);

        faoInstitute = new FaoInstitute();
        faoInstitute.setFullName("This is name of institute");
        faoInstitute.setCurrent(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setMaintainsCollection(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setAccessionCount(1);
        faoInstitute.setUniqueAcceNumbs(true);
        faoInstitute.setCode("Code");

        institutes.add(faoInstitute);
        instituteService.update(institutes);

    }

    @After
    public void teerDown() {
        materialSubRequestRepository.deleteAll();
        materialRequestRepository.deleteAll();
        articleRepository.deleteAll();
    }

    @Test
    public void listRequestsTest() throws Exception {
        LOG.info("Start test-method listRequestsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/requests")
                .contentType(MediaType.APPLICATION_JSON)
                .param("page", "0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(requestService.list(new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))))));

        LOG.info("Test listRequestsTest passed");
    }

    @Test
    public void getRequestTest() throws Exception {
        LOG.info("Start test-method getRequestTest");

        ObjectMapper objectMapper = new ObjectMapper();

        MaterialRequest materialRequestForTest = requestService.list(new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))).getContent().get(0);

        mockMvc.perform(get("/api/v0/requests/r/{uuid}", materialRequestForTest.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(materialRequestForTest)));

        LOG.info("Test getRequestTest passed");
    }

    @Test
    public void reconfirmRequestTest() throws Exception {
        LOG.info("Start test-method reconfirmRequestTest");

        MaterialRequest materialRequestForTest = requestService.list(new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))).getContent().get(0);

        mockMvc.perform(post("/api/v0/requests/r/{uuid}/reconfirm", materialRequestForTest.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        LOG.info("Test reconfirmRequestTest passed");
    }

    @Test
    public void validateRequestTest() throws Exception {
        LOG.info("Start test-method validateRequestTest");

        MaterialRequest materialRequestForTest = requestService.list(new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))).getContent().get(0);
        materialRequestForTest.setSubRequests(materialSubRequestRepository.findAll());
        mockMvc.perform(post("/api/v0/requests/r/{uuid}/validate", materialRequestForTest.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        LOG.info("Test validateRequestTest passed");
    }

    @Test
    public void updatePidTest() throws Exception {
        LOG.info("Start test-method updatePidTest");

        ObjectMapper objectMapper = new ObjectMapper();
        MaterialRequest materialRequestForTest = requestService.list(new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))).getContent().get(0);
        materialRequestForTest.setSubRequests(materialSubRequestRepository.findAll());
        mockMvc.perform(post("/api/v0/requests/r/{uuid}/update-pid", materialRequestForTest.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(requestService.recheckPid(materialRequestForTest))));

        LOG.info("Test updatePidTest passed");
    }

    @Test
    public void listInstituteRequestsTest() throws Exception {
        LOG.info("Start test-method listInstituteRequestsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/requests/{instCode}", materialSubRequest.getInstCode())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(requestService.list(faoInstitute, new PageRequest(0, 10, new Sort(Sort.Direction.DESC, "createdDate"))))));

        LOG.info("Test listInstituteRequestsTest passed");
    }

    @Test
    public void getInstituteRequestTest() throws Exception {
        LOG.info("Start test-method getInstituteRequestTest");

        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(get("/api/v0/requests/{instCode}/r/{uuid}", materialSubRequest.getInstCode(), materialSubRequest.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(requestService.get(faoInstitute, materialSubRequest.getUuid()))));

        LOG.info("Test getInstituteRequestTest passed");
    }

}
