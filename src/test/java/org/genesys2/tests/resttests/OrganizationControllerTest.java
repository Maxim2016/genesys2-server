package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.servlet.controller.rest.OrganizationController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class OrganizationControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(OrganizationControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private Organization organization;
    private Article article;
    private FaoInstitute faoInstitute;
    private List<FaoInstitute> institutes = new ArrayList<>();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        organization = organizationService.create("slugOrganization", "TitleOrganization");

        faoInstitute = new FaoInstitute();
        faoInstitute.setFullName("This is name of institute");
        faoInstitute.setCurrent(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setMaintainsCollection(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setAccessionCount(1);
        faoInstitute.setUniqueAcceNumbs(true);
        faoInstitute.setCode("Code");

        institutes.add(faoInstitute);
        instituteService.update(institutes);

        organization.setMembers(institutes);

        article = new Article();
        article.setSlug("blurp");
        article.setLang("en");
        article.setBody("Body");
        article.setTargetId(2L);
        article.setClassPk(contentService.ensureClassPK(Organization.class));
        List<Article> articleList = new ArrayList<>();
        articleList.add(article);
        contentService.save(articleList);
    }

    @After
    public void tearDown() {

        organizationRepository.deleteAll();
        articleRepository.deleteAll();
        instituteRepository.deleteAll();
    }

    @Test
    public void listOrganizationsTest() throws Exception {
        LOG.info("Start test-method listOrganizationsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/org")
                .contentType(MediaType.APPLICATION_JSON)
                .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(organizationService.list(new PageRequest(0, 10)).getContent())));

        LOG.info("Test listOrganizationsTest is passed");
    }

    @Test
    public void updateOrganizationTest() throws Exception {
        LOG.info("Start test-method updateOrganizationTest");

        ObjectMapper objectMapper = new ObjectMapper();
        organization.setTitle("newTitle");

        mockMvc.perform(post("/api/v0/org")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(organization)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(organization)));

        LOG.info("Test updateOrganizationTest is passed");
    }

    @Test
    public void getOrganizationTest() throws Exception {
        LOG.info("Start test-method getOrganizationTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/org/{shortName}", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(organization)));

        LOG.info("Test getOrganizationTest is passed");
    }

    @Test
    public void getBlurpTest() throws Exception {
        LOG.info("Start test-method getBlurpTest");

        mockMvc.perform(get("/api/v0/org/{shortName}/blurp/{language}", organization.getSlug(), article.getLang())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(article.getBody()));

        LOG.info("Test getBlurpTest is passed");
    }

    @Test
    public void updateBlurpTest() throws Exception {
        LOG.info("Start test-method updateBlurpTest");

        ObjectMapper objectMapper = new ObjectMapper();
        OrganizationController.OrganizationBlurpJson organizationBlurpJson = new OrganizationController.OrganizationBlurpJson();
        organizationBlurpJson.blurp = "newBlurp";
        organizationBlurpJson.locale = "en";

        mockMvc.perform(put("/api/v0/org/{shortName}/blurp", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(organizationBlurpJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(organizationService.updateAbout(organization, organizationBlurpJson.blurp, organizationBlurpJson.summary, organizationBlurpJson.getLocale()))));

        LOG.info("test updateBlurpTest is passed");
    }

    @Test
    public void deleteOrganizationTest() throws Exception {
        LOG.info("Start test-method deleteOrganizationTest");

        ObjectMapper objectMapper = new ObjectMapper();
        organization.setId(null);

        mockMvc.perform(delete("/api/v0/org/{shortName}", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(organization)));

        LOG.info("test deleteOrganizationTest is passed");
    }

    @Test
    public void getOrganizationMembersTest() throws Exception {
        LOG.info("Start test-method getOrganizationMembersTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/org/{shortName}/institutes", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(organizationService.getMembers(organizationService.getOrganization(organization.getSlug())))));

        LOG.info("test getOrganizationMembersTest is passed");
    }

    @Test
    @Transactional
    public void addOrganizationInstitutesTest() throws Exception {
        LOG.info("Start test-method addOrganizationInstitutesTest");

        ObjectMapper objectMapper = new ObjectMapper();
        List<String> instituteList = new ArrayList<>();

        FaoInstitute secondFaoInstitute = new FaoInstitute();
        secondFaoInstitute.setFullName("This is name of second institute");
        secondFaoInstitute.setCurrent(true);
        secondFaoInstitute.setPgrActivity(true);
        secondFaoInstitute.setMaintainsCollection(true);
        secondFaoInstitute.setPgrActivity(true);
        secondFaoInstitute.setAccessionCount(1);
        secondFaoInstitute.setUniqueAcceNumbs(true);
        secondFaoInstitute.setCode("SecondCode");

        List<FaoInstitute> faoInstituteList = new ArrayList<>();
        faoInstituteList.add(secondFaoInstitute);
        instituteService.update(faoInstituteList);

        instituteList.add(faoInstitute.getCode());
        instituteList.add(secondFaoInstitute.getCode());

        mockMvc.perform(post("/api/v0/org/{slug}/add-institutes", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(instituteList)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(true)));

        LOG.info("test addOrganizationInstitutesTest is passed");
    }

    @Test
    @Transactional
    public void setOrganizationInstitutesTest() throws Exception {
        LOG.info("Start test-method setOrganizationInstitutesTest");

        ObjectMapper objectMapper = new ObjectMapper();
        List<String> instituteList = new ArrayList<>();

        FaoInstitute thirdFaoInstitute = new FaoInstitute();
        thirdFaoInstitute.setFullName("This is name of second institute");
        thirdFaoInstitute.setCurrent(true);
        thirdFaoInstitute.setPgrActivity(true);
        thirdFaoInstitute.setMaintainsCollection(true);
        thirdFaoInstitute.setPgrActivity(true);
        thirdFaoInstitute.setAccessionCount(1);
        thirdFaoInstitute.setUniqueAcceNumbs(true);
        thirdFaoInstitute.setCode("SecondCode");

        List<FaoInstitute> faoInstituteList = new ArrayList<>();
        faoInstituteList.add(thirdFaoInstitute);
        instituteService.update(faoInstituteList);

        instituteList.add(faoInstitute.getCode());
        instituteList.add(thirdFaoInstitute.getCode());

        mockMvc.perform(post("/api/v0/org/{slug}/set-institutes", organization.getSlug())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(instituteList)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(true)));

        LOG.info("test setOrganizationInstitutesTest is passed");
    }
}
