package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.servlet.controller.rest.OAuth2Cleanup;
import org.genesys2.server.servlet.controller.rest.TraitsController;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Ignore
public class TraitsControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(TraitsControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private Parameter parameter;
    private Crop crop;
    private ParameterCategory category;
    private Method method;
    private User user;

    @Before
    public void setUp() throws JsonProcessingException, UserException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr19011990");
        user.setName("SYS_ADMIN");

        userService.addUser(user);

        ObjectMapper objectMapper = new ObjectMapper();

        crop = cropService.addCrop("shortName", "Name", "description", "en");

        traitService.addCategory("nameOfParameterCategory", objectMapper.writeValueAsString("en"));

        parameter = traitService.addParameter("rdfUri", crop, "nameOfParameterCategory", "titleOfParameter", objectMapper.writeValueAsString("en"));

        method = traitService.addMethod("rdfUriMethod", "descriptionMethod", objectMapper.writeValueAsString("en"), "unitForMethod", "fieldName", 1, 2, "options", "range", parameter);

        Map<Integer, Boolean> permission = new HashMap<>();
        permission.put(4, true);
        permission.put(1, false);
        permission.put(2, false);
        permission.put(8, false);
        permission.put(16, true);

        aclService.addPermissions(method.getId(), Method.class.getName(), "SYS_ADMIN", true, permission);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        SecurityContextHolder.getContext().setAuthentication(authToken);

    }

    @After
    public void tearDown() {
        methodRepository.deleteAll();
        parameterRepository.deleteAll();
        parameterCategoryRepository.deleteAll();
        cropRepository.deleteAll();
        userPersistence.deleteAll();
        aclObjectIdentityPersistence.deleteAll();
        aclClassPersistence.deleteAll();
        aclSidPersistence.deleteAll();
        aclEntryPersistence.deleteAll();
    }

    @Test
    public void listTraitsTest() throws Exception {
        LOG.info("Start test-method listTraitsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/descriptors")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(OAuth2Cleanup.clean(traitService.listTraits(new PageRequest(0, 50))))));

        LOG.info("Test listTraitsTest passed");
    }

    @Test
    public void listTraitsWithPageParamTest() throws Exception {
        LOG.info("Start test-method listTraitsWithPageParamTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/descriptors/?{page}", 0)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(OAuth2Cleanup.clean(traitService.listTraits(new PageRequest(0, 50))))));

        LOG.info("Test listTraitsWithPageParamTest passed");
    }

    @Test
    public void listMethodsTest() throws Exception {
        LOG.info("Start test-method listMethodsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/methods")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(OAuth2Cleanup.clean(traitService.listMethods(new PageRequest(0, 50))))));

        LOG.info("Test listMethods passed!");
    }

    @Test
    public void listMethodsWithPageParamTest() throws Exception {
        LOG.info("Start test-method listMethodsWithPageParamTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/methods/?{page}", 0)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(OAuth2Cleanup.clean(traitService.listMethods(new PageRequest(0, 50))))));

        LOG.info("Test listMethodsWithPageParamTest passed!");
    }

    @Test
    public void listMyMethodsTest() throws Exception {
        LOG.info("Start test-method listMyMethodsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/mymethods")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(OAuth2Cleanup.clean(traitService.listMyMethods()))));

        LOG.info("Test listMyMethodsTest passed!");
    }

    @Test
    public void listCategoriesTest() throws Exception {
        LOG.info("Start test-method listCategoriesTest");

        ObjectMapper objectmapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/categories")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectmapper.writeValueAsString(traitService.listCategories())));

        LOG.info("Test listMyCategoriesList passed!");
    }

    @Test
    public void createCategoryTest() throws Exception {
        LOG.info("Start test-method createCategoryTest");

        ObjectMapper objectMapper = new ObjectMapper();
        TraitsController.CategoryJson categoryJson = new TraitsController.CategoryJson();
        categoryJson.name = traitService.listCategories().get(0).getName();
        categoryJson.i18n = traitService.listCategories().get(0).getNameL();

        mockMvc.perform(post("/api/v0/category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(categoryJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(traitService.listCategories().get(0))));

        LOG.info("Test createCategoryTest is passed");
    }

    @Test
    public void createPropertyTest() throws Exception {
        LOG.info("Start test-method createPropertyTest");

        ObjectMapper objectMapper = new ObjectMapper();
        TraitsController.PropertyJson propertyJson = new TraitsController.PropertyJson();
        propertyJson.category = "nameOfParameterCategory";
        propertyJson.rdfUri = "rdfUri";
        propertyJson.i18n = objectMapper.writeValueAsString("en");
        propertyJson.title = "titleOfParameter";

        mockMvc.perform(post("/api/v0/{crop}/property", crop.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(propertyJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(parameter)));

        LOG.info("Test createPropertyTest is passed");
    }

    @Test
    public void createMethodTest() throws Exception {
        LOG.info("Start test-method createMethodTest");

        ObjectMapper objectMapper = new ObjectMapper();

        TraitsController.MethodJson methodJson = new TraitsController.MethodJson();
        methodJson.rdfUri = method.getRdfUri();
        methodJson.description = method.getMethod();
        methodJson.fieldName = method.getFieldName();
        methodJson.fieldSize = 10;
        methodJson.fieldType = method.getFieldType();
        methodJson.i18n = objectMapper.writeValueAsString("en");
        methodJson.options = method.getOptions();
        methodJson.unit = method.getUnit();
        methodJson.range = method.getRange();

        mockMvc.perform(post("/api/v0/{propertyId}/method", parameter.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(methodJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(method)));

        LOG.info("Test createMethodTest");
    }

}
