package org.genesys2.tests.resttests;

import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.genesys2.tests.unit.FooBar;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@Configuration
@TestPropertySource({ "classpath:application.properties", "classpath:spring/spring.properties" })
@ActiveProfiles("dev")
@ContextConfiguration(name = "root", classes = { FooBar.class, JpaDataConfig.class, HazelcastConfig.class }, initializers = PropertyPlacholderInitializer.class)
public abstract class SpringTest {


}