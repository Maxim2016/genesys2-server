package org.genesys2.tests.resttests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.MetadataMethod;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthClientType;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.servlet.controller.rest.DatasetController;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.DefaultAuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Ignore
public class DatasetControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(DatasetControllerTest.class);

    @Autowired
    public WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private Metadata metadata;
    private FaoInstitute faoInstitute;
    private List<FaoInstitute> institutes = new ArrayList<>();
    private User user = new User();
    private OAuth2AccessToken accessToken;
    private OAuthRefreshToken refreshToken;
    private OAuthClientDetails oAuthClientDetails;
    private Method method;
    private Crop crop;
    private ParameterCategory category;
    private Parameter parameter;
    private MetadataMethod metadataMethod;
    private Accession accession;
    private Country country;

    @Before
    public void setup() throws UserException, JsonProcessingException, InterruptedException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        oAuthClientDetails = clientDetailsService.addClientDetails("title", "description", "redirectUri", 1, 2, OAuthClientType.SERVICE);

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr19011990");
        user.setName("SYS_ADMIN");

        userService.addUser(user);

        faoInstitute = new FaoInstitute();
        faoInstitute.setFullName("This is name of institute");
        faoInstitute.setCurrent(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setMaintainsCollection(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setAccessionCount(1);
        faoInstitute.setUniqueAcceNumbs(true);
        faoInstitute.setCode("Code");

        institutes.add(faoInstitute);
        instituteService.update(institutes);

        ObjectMapper objectMapper = new ObjectMapper();

        crop = cropService.addCrop("shortName", "Name", "description", "en");

        traitService.addCategory("nameOfParameterCategory", objectMapper.writeValueAsString("en"));

        parameter = traitService.addParameter("rdfUri", crop, "nameOfParameterCategory", "titleOfParameter", objectMapper.writeValueAsString("en"));

        method = traitService.addMethod("rdfUriMethod", "descriptionMethod", objectMapper.writeValueAsString("en"), "unitForMethod", "fieldName", 1, 2, "options", "range", parameter);

        metadata = datasetService.addDataset(faoInstitute, "TitleMetaData", "Description metaData");
        Map<Integer, Boolean> permission = new HashMap<>();

        metadataMethod = new MetadataMethod();
        metadataMethod.setMetadata(metadata);
        metadataMethod.setMethodId(method.getId());
        metadataMethodRepository.save(metadataMethod);

        permission.put(4, true);
        permission.put(1, false);
        permission.put(2, false);
        permission.put(8, false);
        permission.put(16, true);

        aclService.addPermissions(metadata.getId(), Metadata.class.getName(), "SYS_ADMIN", true, permission);

        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        authorizationParameters.put("scope", "read");
        authorizationParameters.put("username", user.getName());
        authorizationParameters.put("client_id", user.getName());
        authorizationParameters.put("grant", user.getPassword());

        DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
        authorizationRequest.setApproved(true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        authorizationRequest.setAuthorities(authorities);

        HashSet<String> resourceIds = new HashSet<String>();
        resourceIds.add(user.getName());
        authorizationRequest.setResourceIds(resourceIds);
        oAuthClientDetails.setResourceIds(user.getName());
        AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        OAuth2Authentication authenticationRequest = new OAuth2Authentication(authorizationRequest, authenticationToken);
        authenticationRequest.setAuthenticated(true);

        accessToken = ((DefaultTokenServices) tokenServices).createAccessToken(authenticationRequest);
        refreshToken = new OAuthRefreshToken();
        refreshToken.setValue("Value");
        refreshToken.setClientId(user.getName());
        refreshTokenPersistence.save(refreshToken);

        Taxonomy2 taxonomy2 = taxonomyService.internalEnsure(String.valueOf(0), "Species taxonomy", "SpAuthor", "SubTaxa", "SubtAuthor");

        accession = new Accession();
        accession.setVersion(1);
        accession.setInstitute(faoInstitute);
        accession.setAccessionName("NameAccession");
        accession.setHistoric(false);
        accession.setTaxonomy(taxonomy2);

        AccessionId accessionId = new AccessionId();
        accessionId.setVersion(1);

        accession.setAccessionId(accessionId);

        List<Accession> accessionList = new ArrayList<>();
        accessionList.add(accession);
        genesysService.addAccessions(accessionList);

        SecurityContextHolder.getContext().setAuthentication(authenticationRequest);
    }

    @After
    public void tearDown() {
        methodRepository.deleteAll();
        parameterRepository.deleteAll();
        parameterCategoryRepository.deleteAll();
        cropRepository.deleteAll();
        userPersistence.deleteAll();
        accessionRepository.deleteAll();
        taxonomy2Repository.deleteAll();
        instituteRepository.deleteAll();
        metadataMethodRepository.deleteAll();
        metadataRepository.deleteAll();
    }

    @Test
    public void listDatasetsTest() throws Exception {
        LOG.info("Start test-method listDatasetsTest");

        mockMvc.perform(get("/api/v0/datasets")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].instituteCode", is("Code")))
                .andExpect(jsonPath("$[0].title", is("TitleMetaData")))
                .andExpect(jsonPath("$[0].description", is("Description metaData")))
                .andExpect(jsonPath("$[0].uuid", is(notNullValue())));

        LOG.info("Test listDatasetsTest passed");
    }

    @Test
    public void createDatasetTest() throws Exception {
        LOG.info("Start test-method createDatasetTest");

        metadataMethodRepository.deleteAll();
        metadataRepository.deleteAll();

        DatasetController.MetadataJson metadataJson = new DatasetController.MetadataJson();
        metadataJson.title = metadata.getTitle();
        metadataJson.institute = metadata.getInstituteCode();
        metadataJson.description = metadata.getDescription();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/datasets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(metadataJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.instituteCode", is("Code")))
                .andExpect(jsonPath("$.title", is("TitleMetaData")))
                .andExpect(jsonPath("$.description", is("Description metaData")))
                .andExpect(jsonPath("$.uuid", is(notNullValue())));

        LOG.info("Test createDatasetTest passed");
    }

    @Test
    public void getCropTest() throws Exception {
        LOG.info("Start test-method getCropTest");

        mockMvc.perform(get("/api/v0/datasets/{metadataId}", metadata.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.instituteCode", is("Code")))
                .andExpect(jsonPath("$.title", is("TitleMetaData")))
                .andExpect(jsonPath("$.description", is("Description metaData")))
                .andExpect(jsonPath("$.uuid", is(notNullValue())));

        LOG.info("Test getCropTest passed");
    }

    @Test
    public void getMetadataMethodsTest() throws Exception {
        LOG.info("Start test-method getMetadataMethodsTest");

        mockMvc.perform(get("/api/v0/datasets/{metadataId}/methods", metadata.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].version", is(0)))
                .andExpect(jsonPath("$[0].rdfUri", is("rdfUriMethod")))
                .andExpect(jsonPath("$[0].method", is("descriptionMethod")))
                .andExpect(jsonPath("$[0].uuid", is(notNullValue())))
                .andExpect(jsonPath("$[0].unit", is("unitForMethod")))
                .andExpect(jsonPath("$[0].options", is("options")))
                .andExpect(jsonPath("$[0].fieldName", is("fieldName")))
                .andExpect(jsonPath("$[0].fieldType", is(1)))
                .andExpect(jsonPath("$[0].range", is("range")))
                .andExpect(jsonPath("$[0].localMethodMap", is(Map.class)))
                .andExpect(jsonPath("$[0].dataType", is("java.lang.Double")))
                .andExpect(jsonPath("$[0].coded", is(true)));

        LOG.info("Test getMetadataMethodsTest passed");
    }

    @Test
    public void upsertMetadataDataTest() throws Exception {
        LOG.info("Start test-method upsertMetadataDataTest");

        ObjectMapper objectMapper = new ObjectMapper();
        DatasetController.DataJson dataJson = new DatasetController.DataJson();
        dataJson.genus = String.valueOf(accession.getTaxGenus());
        dataJson.acceNumb = accession.getAccessionName();
        dataJson.instCode = accession.getInstituteCode();

        mockMvc.perform(put("/api/v0/datasets/{metadataId}/data", metadata.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dataJson)))
                .andExpect(status().isOk());

        LOG.info("Test upsertMetadataDataTest passed");
    }
}
